import os
from os.path import abspath, dirname

__author__ = 'Andre'

PROJECT_ROOT = abspath(dirname(__file__) + '/../').replace('\\', '/')

CURRENT_CONTROL_SET = 'localhost'

DB_ENGINE_TPL = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'space-jump',
    'USER': 'root',
    'PASSWORD': '1qaz3wasql',
    'HOST': 'localhost',
    'PORT': '',
    'OPTIONS': {
        'init_command': 'SET storage_engine=INNODB',
    },
}

CONTROL_SETS_TABLE = {
    'localhost': {
        'www_root': PROJECT_ROOT,
        'api_id': {
            'vk': 2896510,
            'fb': 148797118628286,
            'ok': 1222656,
            'mm': 708312,
        },
        'api_key': {
            'ok': 'CBAJKDBBABABABABA',
            'mm': '20c5587c1819d976740277674227c727',
        },
        'api_secret': {
            'vk': 'te9HFfc0jSJTAOP3FUW0',
            'fb': 'ab0ea44d3db97f8c6268920ed3348b7e',
            'ok': '0F598797205C387FC01502B8',
            'mm': '8db19fb100e5220e8e09b8ddfd9cf5b6',
        },
        'db': {
            'default': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump',
                'HOST': '127.0.0.1',
            }.items()),
            'facebook_com': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-fb',
                'HOST': '127.0.0.1',
            }.items()),
            'odnoklassniki_ru': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-ok',
                'HOST': '127.0.0.1',
            }.items()),
            'my_mail_ru': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-mm',
                'HOST': '127.0.0.1',
            }.items()),
        },
    },
    'space-jump.hub.webkraken.ru': {
        'www_root': os.path.abspath(PROJECT_ROOT + '/../'),
        'api_id': {
            'vk': 2896510,
            'fb': 148797118628286,
            'ok': 'doxphexvfekrvfkcw0fohrgwigfwbxkiybjrr',
            'mm': 708312,
        },
        'api_key': {
            'ok': 'CBAGDEAMABABABABA',
            'mm': '20c5587c1819d976740277674227c727',
        },
        'api_secret': {
            'vk': 'te9HFfc0jSJTAOP3FUW0',
            'fb': 'ab0ea44d3db97f8c6268920ed3348b7e',
            'ok': '5B135C00141D7D6B917EFDB2',
            'mm': '8db19fb100e5220e8e09b8ddfd9cf5b6',
        },
        'db': {
            'default': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump',
                'HOST': 'localhost',
            }.items()),
            'facebook_com': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-fb',
                'HOST': 'localhost',
            }.items()),
            'odnoklassniki_ru': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-ok',
                'HOST': 'localhost',
            }.items()),
            'my_mail_ru': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-mm',
                'HOST': 'localhost',
            }.items()),
        },
    },
    'space-jump-rc.hub.webkraken.ru': {
        'www_root': PROJECT_ROOT,
        'api_id': {
            'vk': 3516773,
            'fb': 148797118628286,
            'ok': 1222656,
            'mm': 708312,
        },
        'api_key': {
            'ok': 'CBAJKDBBABABABABA',
            'mm': '20c5587c1819d976740277674227c727',
        },
        'api_secret': {
            'vk': 'SkMvX7dEQWsHULUIvQb4',
            'fb': 'ab0ea44d3db97f8c6268920ed3348b7e',
            'ok': '0F598797205C387FC01502B8',
            'mm': '8db19fb100e5220e8e09b8ddfd9cf5b6',
        },
        'db': {
            'default': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump',
                'HOST': 'localhost',
            }.items()),
            'facebook_com': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-fb',
                'HOST': 'localhost',
            }.items()),
            'odnoklassniki_ru': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-ok',
                'HOST': 'localhost',
            }.items()),
            'my_mail_ru': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-mm',
                'HOST': 'localhost',
            }.items()),
        },
    },
    'spacediver.webkraken.ru': {
        'www_root': '/var/www/spacediver.webkraken.ru/',
        'api_id': {
            'vk': 3642981,
            'fb': 148797118628286,
            'ok': 0,
            'mm': 708312,
        },
        'api_key': {
            'ok': 'CBAQNQPLABABABABA',
            'mm': '20c5587c1819d976740277674227c727',
        },
        'api_secret': {
            'vk': '8oyJzPmrN2pfTflhKyDh',
            'fb': 'ab0ea44d3db97f8c6268920ed3348b7e',
            'ok': 'D74433AAA20805AD89A6B55B',
            'mm': '8db19fb100e5220e8e09b8ddfd9cf5b6',
        },
        'db': {
            'default': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump',
                'HOST': '/var/run/mysqld/mysqld.sock',
            }.items()),
            'facebook_com': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-fb',
                'HOST': '/var/run/mysqld/mysqld.sock',
            }.items()),
            'odnoklassniki_ru': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-ok',
                'HOST': '/var/run/mysqld/mysqld.sock',
            }.items()),
            'my_mail_ru': dict(DB_ENGINE_TPL.items() + {
                'NAME': 'space-jump-mm',
                'HOST': '/var/run/mysqld/mysqld.sock',
            }.items()),
        },
    }
}

WWW_ROOT = CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['www_root']
