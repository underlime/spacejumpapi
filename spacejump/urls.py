from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from rest_api.views import api_urls
from rest_api.views.fb_payments import FbPaymentsView
from rest_api.views.game_result_testing_view import GameResultTestingView
from rest_api.views.ok_payments import OkPaymentsView
from rest_api.views.vk_payments import VkPaymentsView
from rest_api.views.mm_payments import MmPaymentsView
from rest_api.views.testing_view import *


admin.autodiscover()
urlpatterns = patterns(
    '',
    url(r'^api/(?P<soc_net>[\w\.]+)/', include(api_urls.getUrls())),
    url(r'^api/vk\.com/buy\.coins/$', VkPaymentsView.as_view()),
    url(r'^api/facebook\.com/buy\.coins/$', FbPaymentsView.as_view()),
    url(r'^api/odnoklassniki\.ru/buy\.coins/$', OkPaymentsView.as_view()),
    url(r'^api/my\.mail\.ru/buy\.coins/$', MmPaymentsView.as_view()),

    url(r'^backoffice/', include(admin.site.urls)),
    url(r'^backoffice/api-testing/((?P<method_code>[\w\./-]+?)(/)?)?$', TestingView.as_view()),
    url(r'^backoffice/result-testing(/)?$', GameResultTestingView.as_view()),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    )
