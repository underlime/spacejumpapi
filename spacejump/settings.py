# Django settings for spacejump project.
from spacejump.settings_control_sets import CONTROL_SETS_TABLE, CURRENT_CONTROL_SET, WWW_ROOT

isLocalHost = (CURRENT_CONTROL_SET == 'localhost')

DEBUG = (CURRENT_CONTROL_SET in ('localhost', 'space-jump.hub.webkraken.ru'))
TEMPLATE_DEBUG = DEBUG
FRIENDS_DEBUG = DEBUG and isLocalHost
SQL_DEBUG = False

if isLocalHost:
    ALLOWED_HOSTS = (
        '127.0.0.1',
        'localhost',
    )
else:
    ALLOWED_HOSTS = (
        '.webkraken.ru',
    )

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASE_ROUTERS = ['spacejump.db_routers.DbRouter']
DATABASES = CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['db']

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'OPTIONS': {
            'MAX_ENTRIES': 1000000,
            'KEY_PREFIX': 'space_jump',
        }
    }
}


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru-RU'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = WWW_ROOT + '/media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = WWW_ROOT + '/api/static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/api/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '!%s402sv@3sbrw=(%x9slv*6q%+*t@==xz8kfjpd4inptp2ib3'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'rest_api.middleware.RouteDb',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'spacejump.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'spacejump.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'rest_api',
    'south',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'file': {
            'format': '\n%(asctime)s\n%(levelname)s %(process)d\n%(pathname)s: (%(lineno)d)\n%(message)s\n'
        },
    },
    'handlers': {
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': WWW_ROOT + '/.error.log',
            'formatter': 'file',
        },
        'stream': {
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        'django': {
            'handlers': ('logfile', 'stream'),
            'level': 'ERROR',
            'propagate': True,
        },
        'rest_api': {
            'handlers': ('logfile', 'stream'),
            'level': 'ERROR',
            'propagate': True
        },
        'django.request': {
            'handlers': ('stream',),
            'level': 'INFO' if DEBUG else 'CRITICAL',
            'propagate': True,
        },
        'django.db.backends': {
            'handlers': ('stream',),
            'level': 'DEBUG' if SQL_DEBUG else 'CRITICAL',
            'propagate': True,
        },
    }
}

GAME_SETTINGS = {
    'result_salt': 'quxisog.',
    'token_salt': 'cyhypaz_',
    'trophy_probability': 0.05,
    'suite_damage': {
        'enabled_damage': True,
        'restore_by_win': {
            'easy': 0,
            'medium': 0,
            'hard': 0,
            'endless': 0,
        },
        'repair_price': 100,
        'auto_repair_seconds': 2400,
        'free_jumps': 5,
    },
    'explore_coins': {
        'min': 30,
        'max': 100,
    },
    'friends_gifts': {
        'gems': 1,
        'coins': 500,
        'parachute': 2,
        'rocket': 2,
        'resurrection': 2,
    },
    'news_like_coins': 200,
    'max_capsules_to_explore': 5,
    'gems_rate': 1000,
    'first_day_prize': 250,
    'last_day_prize': 2,
    'soc_net': {
        'vk_com': {
            'api_id': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_id']['vk'],
            'api_secret': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_secret']['vk'],
            'coins_picture': 'http://cosmodiver.static.evast.ru/images/coins.png',
            'gems_picture': 'http://cosmodiver.static.evast.ru/images/gems.png',
            'actions_prizes': {
                'favorites': 1000,
                'group': 500,
            },
            'coins_prices': [
                {'coins': 250, 'price': 1, 'image': 'SPACE_MONEY_TIER_1'},
                {'coins': 1500, 'price': 5, 'image': 'SPACE_MONEY_TIER_2'},
                {'coins': 4000, 'price': 10, 'image': 'SPACE_MONEY_TIER_3'},
                {'coins': 10000, 'price': 20, 'image': 'SPACE_MONEY_TIER_4'},
                {'coins': 50000, 'price': 50, 'image': 'SPACE_MONEY_TIER_5'},
                {'coins': 200000, 'price': 100, 'image': 'SPACE_MONEY_TIER_6'},
            ],
            'gems_prices': [
                {'gems': 7, 'price': 1, 'image': 'SPACE_GEM_TIER_1'},
                {'gems': 39, 'price': 5, 'image': 'SPACE_GEM_TIER_2'},
                {'gems': 84, 'price': 10, 'image': 'SPACE_GEM_TIER_3'},
                {'gems': 455, 'price': 50, 'image': 'SPACE_GEM_TIER_4'},
                {'gems': 1050, 'price': 100, 'image': 'SPACE_GEM_TIER_5'},
                {'gems': 2100, 'price': 150, 'image': 'SPACE_GEM_TIER_6'},
            ]
        },
        'facebook_com': {
            'api_id': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_id']['fb'],
            'api_secret': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_secret']['fb'],
            'coins_picture': 'http://cosmodiver.static.evast.ru/images/coins.png',
            'gems_picture': 'http://cosmodiver.static.evast.ru/images/gems.png',
            'actions_prizes': {
                'favorites': None,
                'group': None,
            },
            'coins_prices': [
                {'coins': 500, 'price': 1, 'image': 'SPACE_MONEY_TIER_1'},
                {'coins': 1000, 'price': 25, 'image': 'SPACE_MONEY_TIER_2'},
                {'coins': 5000, 'price': 25, 'image': 'SPACE_MONEY_TIER_3'},
                {'coins': 10000, 'price': 50, 'image': 'SPACE_MONEY_TIER_4'},
                {'coins': 25000, 'price': 150, 'image': 'SPACE_MONEY_TIER_5'},
                {'coins': 50000, 'price': 300, 'image': 'SPACE_MONEY_TIER_6'},
            ],
            'gems_prices': [
                {'gems': 1, 'price': 1, 'image': 'SPACE_MONEY_TIER_1'},
                {'gems': 5, 'price': 5, 'image': 'SPACE_MONEY_TIER_2'},
                {'gems': 10, 'price': 10, 'image': 'SPACE_MONEY_TIER_3'},
                {'gems': 50, 'price': 50, 'image': 'SPACE_MONEY_TIER_4'},
                {'gems': 100, 'price': 100, 'image': 'SPACE_MONEY_TIER_5'},
                {'gems': 250, 'price': 150, 'image': 'SPACE_MONEY_TIER_6'},
            ]
        },
        'odnoklassniki_ru': {
            'api_id': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_id']['ok'],
            'api_key': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_key']['ok'],
            'api_secret': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_secret']['ok'],
            'coins_picture': 'http://cosmodiver.static.evast.ru/images/coins.png',
            'gems_picture': 'http://cosmodiver.static.evast.ru/images/gems.png',
            'actions_prizes': {
                'favorites': 1000,
                'group': 500,
            },
            'coins_prices': [
                {'coins': 250, 'price': 8, 'image': 'SPACE_MONEY_TIER_1'},
                {'coins': 1500, 'price': 40, 'image': 'SPACE_MONEY_TIER_2'},
                {'coins': 4000, 'price': 80, 'image': 'SPACE_MONEY_TIER_3'},
                {'coins': 10000, 'price': 160, 'image': 'SPACE_MONEY_TIER_4'},
                {'coins': 50000, 'price': 400, 'image': 'SPACE_MONEY_TIER_5'},
                {'coins': 200000, 'price': 800, 'image': 'SPACE_MONEY_TIER_6'},
            ],
            'gems_prices': [
                {'gems': 7, 'price': 8, 'image': 'SPACE_GEM_TIER_1'},
                {'gems': 39, 'price': 40, 'image': 'SPACE_GEM_TIER_2'},
                {'gems': 84, 'price': 80, 'image': 'SPACE_GEM_TIER_3'},
                {'gems': 455, 'price': 400, 'image': 'SPACE_GEM_TIER_4'},
                {'gems': 1050, 'price': 800, 'image': 'SPACE_GEM_TIER_5'},
                {'gems': 2100, 'price': 1200, 'image': 'SPACE_GEM_TIER_6'},
            ]
        },
        'my_mail_ru': {
            'api_id': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_id']['mm'],
            'api_key': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_key']['mm'],
            'api_secret': CONTROL_SETS_TABLE[CURRENT_CONTROL_SET]['api_secret']['mm'],
            'coins_picture': 'http://cosmodiver.static.evast.ru/images/coins.png',
            'gems_picture': 'http://cosmodiver.static.evast.ru/images/gems.png',
            'actions_prizes': {
                'favorites': 1000,
                'group': 500,
            },
            'coins_prices': [
                {'coins': 250, 'price': 8, 'image': 'SPACE_MONEY_TIER_1'},
                {'coins': 1500, 'price': 40, 'image': 'SPACE_MONEY_TIER_2'},
                {'coins': 4000, 'price': 80, 'image': 'SPACE_MONEY_TIER_3'},
                {'coins': 10000, 'price': 160, 'image': 'SPACE_MONEY_TIER_4'},
                {'coins': 50000, 'price': 400, 'image': 'SPACE_MONEY_TIER_5'},
                {'coins': 200000, 'price': 800, 'image': 'SPACE_MONEY_TIER_6'},
            ],
            'gems_prices': [
                {'gems': 7, 'price': 8, 'image': 'SPACE_GEM_TIER_1'},
                {'gems': 39, 'price': 40, 'image': 'SPACE_GEM_TIER_2'},
                {'gems': 84, 'price': 80, 'image': 'SPACE_GEM_TIER_3'},
                {'gems': 455, 'price': 400, 'image': 'SPACE_GEM_TIER_4'},
                {'gems': 1050, 'price': 800, 'image': 'SPACE_GEM_TIER_5'},
                {'gems': 2100, 'price': 1200, 'image': 'SPACE_GEM_TIER_6'},
            ]
        },
    }
}
