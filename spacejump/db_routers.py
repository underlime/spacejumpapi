__author__ = 'Andre'


class DbRouter(object):
    VK_COM = 'default'
    FACEBOOK_COM = 'facebook_com'
    ODNOKLASSNIKI_RU = 'odnoklassniki_ru'
    MY_MAIL_RU = 'my_mail_ru'

    currentDb = 'default'

    def db_for_read(self, model, **hints):
        return self.currentDb

    def db_for_write(self, model, **hints):
        return self.currentDb

    def allow_relation(self, obj1, obj2, **hints):
        return True

    def allow_syncdb(self, db, model):
        return True
