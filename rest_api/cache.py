# coding=utf-8
from django.core.cache import cache
from rest_api.logic.logic import Logic
from spacejump.db_routers import DbRouter


__author__ = 'Andre'
CACHE_PREFIX = u'space-jump_'


def buildCacheKey(baseKey, params=None):
    if params is None:
        params = []
    start = 0
    length = len(params)
    if length and isinstance(params[0], Logic):
        start = 1
    cacheKey = CACHE_PREFIX + unicode(DbRouter.currentDb) + unicode(baseKey)
    for i in range(start, length):
        cacheKey += '_' + unicode(params[i])
    return cacheKey


def setItem(baseKey, value, ttl):
    cacheKey = buildCacheKey(baseKey)
    return cache.set(cacheKey, value, ttl)


def getItem(baseKey, default=None):
    cacheKey = buildCacheKey(baseKey)
    data = cache.get(cacheKey)
    if data is None:
        return default
    else:
        return data


def cachedFunction(baseKey, cacheTtl=86400):
    """
    Обертка декоратора для передачи времени кэширования
    """
    def cachedDecorator(func):
        """
        Декоратор для функций, требующих кэширования результата.
        В функциях с этим декоратором не поддерживаются именованные аргументы!
        """
        def getDataFromCache(args, cacheKey):
            isLogic = (args and isinstance(args[0], Logic))
            if (isLogic and args[0].cacheEnabled) or not isLogic:
                data = cache.get(cacheKey)
                return data
            else:
                return None

        def wrapper(*args):
            cacheKey = buildCacheKey(baseKey, args)
            data = getDataFromCache(args, cacheKey)
            if data is not None:
                return data
            data = func(*args)
            cache.set(cacheKey, data, cacheTtl)
            return data
        return wrapper
    return cachedDecorator


def delDataFromCache(baseKey, params):
    """
    Удалить данные из кэша
    """
    cacheKey = buildCacheKey(baseKey, params)
    return cache.delete(cacheKey)


