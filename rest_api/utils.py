from django.forms import model_to_dict

__author__ = 'Andre'


def buildModelResponseStruct(modelCollection, idAttrName='id'):
    result = {}
    for record in modelCollection:
        result[getattr(record, idAttrName)] = model_to_dict(record)
    return result
