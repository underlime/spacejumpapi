# coding=utf-8
from django import template
from django.conf import settings

__author__ = 'Andre'
register = template.Library()


@register.filter
def key(data, key):
    if not isinstance(data, (dict, list, dict)):
        return settings.TEMPLATE_STRING_IF_INVALID
    try:
        return data[key]
    except:
        return settings.TEMPLATE_STRING_IF_INVALID


@register.filter
def tuple_key(data, key):
    if not isinstance(data, (list, tuple)):
        return settings.TEMPLATE_STRING_IF_INVALID
    try:
        for record in data:
            if record[0] == key:
                return record[1]
        return settings.TEMPLATE_STRING_IF_INVALID
    except:
        return settings.TEMPLATE_STRING_IF_INVALID


@register.filter
def print_r(data, pref=''):
    if not isinstance(data, (dict, list, dict)):
        return settings.TEMPLATE_STRING_IF_INVALID
    DELIMITER = ' : '
    ans = ''
    if isinstance(data, dict):
        for key in data:
            ans += pref + key + DELIMITER
            if isinstance(data[key], (dict, list, dict)):
                ans += '\n' + print_r(data[key], pref+'    ')
            else:
                ans += unicode(data[key])
            ans += '\n'
    else:
        c = 0
        for item in data:
            ans += pref + unicode(c) + DELIMITER
            if isinstance(item, (dict, list, dict)):
                ans += '\n' + print_r(item, pref+'    ')
            else:
                ans += unicode(item)
            ans += '\n'
            c += 1
    return ans
