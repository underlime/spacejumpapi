from django.forms import model_to_dict
from rest_api.logic.repair import repairSuite
from rest_api.response.achievements import packAchvData


__author__ = 'Andre'


def repairSuiteResp(userId, hpPoints, currency, socNet):
    suiteParams, newCoins, achv, userInfo = repairSuite(userId, hpPoints, currency)
    resp = {
        'suite_params': model_to_dict(suiteParams),
    }

    if achv and userInfo:
        resp.update(packAchvData(achv, userInfo))
        socNet.setUserLevel(userInfo.soc_net_id, userInfo.stars)
    else:
        resp['user'] = {
            currency: newCoins
        }

    return resp
