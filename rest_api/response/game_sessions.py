# coding=utf-8
import base64
import hashlib
import json
from django.forms import model_to_dict
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.game_sessions import initGameSession, finishGameSession
from rest_api.logic.game_sessions_constants import SIG_FIELDS_LIST
from rest_api.response.trophies import getTrophiesCollectionsList
from spacejump.settings import GAME_SETTINGS


__author__ = 'Andre'


def initGameSessionResp(userId, difficulty):
    gameSession, suiteParams = initGameSession(userId, difficulty)
    return {
        'game_session': model_to_dict(gameSession),
        'suite_params': model_to_dict(suiteParams),
    }


def finishGameSessionResp(userId, result, socNet):
    data = _decodeGameResult(result)

    itemsUsed = data.get('items_used')
    if not isinstance(itemsUsed, list):
        raise NotCorrectError('Wrong used items format')

    iRange = range(0, len(itemsUsed))
    for i in iRange:
        if not str(itemsUsed[i]).isdigit():
            message = 'Incorrect item: {0}'.format(itemsUsed[i])
            raise NotCorrectError(message)
        else:
            itemsUsed[i] = long(itemsUsed[i])

    for fieldName in SIG_FIELDS_LIST:
        if not str(data.get(fieldName)).isdigit():
            message = 'Incorrect {0} value'.format(fieldName)
            raise NotCorrectError(message)
        else:
            data[fieldName] = long(data[fieldName])

    data = finishGameSession(userId, data, itemsUsed)
    respData = {
        'game_session_finish': True,
        'user': model_to_dict(data['user_info'])
    }

    if data['new_score']:
        respData['new_max_score'] = True

    if data['trophy']:
        respData.update(getTrophiesCollectionsList())
        respData.update({
            'users_trophies': {
                '_add': {
                    data['trophy'].id: model_to_dict(data['trophy'])
                }
            }
        })

    if data['suite_record']:
        respData.update({
            'suite_params': model_to_dict(data['suite_record'])
        })

    if data['inventory']:
        respData.update({
            'inventory': {
                '_update': {},
                '_delete': [],
            }
        })

        for invData in data['inventory']:
            if invData.count:
                respData['inventory']['_update'][invData.id] = model_to_dict(invData)
            else:
                respData['inventory']['_delete'].append(invData.id)

    if data['user_achievements_list']:
        respData.update({
            'user_achievements': {
                '_add': {}
            }
        })

        for userAchv in data['user_achievements_list']:
            respData['user_achievements']['_add'][userAchv.id] = model_to_dict(userAchv)

        socNet.setUserLevel(data['user_info'].soc_net_id, data['user_info'].stars)

    return respData


def _decodeGameResult(result):
    try:
        jsonData = base64.b64decode(result)
    except TypeError:
        raise NotCorrectError('Result parse error')

    try:
        data = json.loads(jsonData)
    except ValueError:
        raise NotCorrectError('Result data parse error')

    _checkResultSignature(data)
    return data


def _checkResultSignature(data):
    """
    Функция проверки подписи результата прыжка.
    Создание подписи должно опираться на этот алгоритм
    """
    sig = data.get('sig')
    if not sig:
        raise NotCorrectError('Signature required')

    sigBase = ''
    for field in SIG_FIELDS_LIST:
        value = data.get(field)
        if value is None:
            raise NotCorrectError(field + ' required')
        sigBase += str(value) + ':'

    value = data.get('items_used')
    if value is None:
        raise NotCorrectError('items_used required')
    sigBase += ','.join(list(value)) + ':'

    sigBase += GAME_SETTINGS['result_salt']
    sigMd5 = hashlib.md5()
    sigMd5.update(sigBase)
    exceptedSig = sigMd5.hexdigest()

    if sig != exceptedSig:
        raise NotCorrectError('Signature is invalid')
