# coding=utf-8
from django.forms import model_to_dict
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.lists import ListsLogic
from rest_api.logic.news import getUserNews, getNewsRead
from rest_api.logic.repair import getSuiteParams
from rest_api.logic.user import UserLogic, PRIZES_ACTIONS_TABLE
from rest_api.models import Users
from spacejump.settings import GAME_SETTINGS

__author__ = 'Andre'


def getFullUserInfo(socNetId, baseInfoOnly, handleVisitsInfo=False):
    """
    Извлечь полную информацию о пользователе
    @param socNetId int
    @return dict
    """

    if not str(socNetId).isdigit():
        raise NotCorrectError('Wrong soc net id: ' + str(socNetId))

    if not str(baseInfoOnly).isdigit():
        raise NotCorrectError('Wrong base info flag')

    socNetId = long(socNetId)
    baseInfoOnly = int(baseInfoOnly)

    userLogic = UserLogic()
    if handleVisitsInfo:
        prizeType = userLogic.handleLastVisitInfo(socNetId)
    else:
        prizeType = None

    userInfo = userLogic.getUserInfo(socNetId)
    inventory = userLogic.getInventory(userInfo['id'])
    userAchievements = userLogic.getUserAchievements(userInfo['id'])

    if not baseInfoOnly:
        listsLogic = ListsLogic()
        achievements = listsLogic.getAchievementsList()
        items = listsLogic.getItemsList()
        suiteParams, itemInfo = getSuiteParams(userInfo['id'])
        userInfo['max_capsules_to_explore'] = GAME_SETTINGS['max_capsules_to_explore']
        userInfo['gems_rate'] = GAME_SETTINGS['gems_rate']
        userInfo['first_day_prize'] = GAME_SETTINGS['first_day_prize']
        userInfo['last_day_prize'] = GAME_SETTINGS['last_day_prize']
        userInfo['prizes_actions_table'] = PRIZES_ACTIONS_TABLE
        if prizeType:
            userInfo['day_prize_type'] = prizeType
        userInfo['friends_gifts'] = GAME_SETTINGS['friends_gifts']
        userInfo['news_like_coins'] = GAME_SETTINGS['news_like_coins']
        news = getUserNews(userInfo['id'])
        newsRead = getNewsRead(userInfo['id'])
    else:
        achievements = None
        items = None
        suiteParams = None
        news = None
        newsRead = None
    return packRespFullUserInfo(userInfo, inventory, userAchievements, items, achievements, suiteParams, news, newsRead)


def packRespFullUserInfo(userInfo, inventory=None, userAchievements=None, items=None, achievements=None,
                         suiteParams=None, news=None, newsRead=None):
    if inventory is None:
        inventory = {}

    if userAchievements is None:
        userAchievements = {}

    if items is None:
        items = {}

    if achievements is None:
        achievements = {}

    if news is None:
        news = {}

    if newsRead is None:
        newsRead = []

    data = {
        'user': userInfo,
        'items': {
            '_init': items,
        },
        'inventory': {
            '_init': inventory,
        },
        'achievements': {
            '_init': achievements,
        },
        'user_achievements': {
            '_init': userAchievements,
        },
        'user_news': {
            '_init': news,
        },
        'news_read': {
            '_init': newsRead
        },
    }

    if suiteParams:
        data['suite_params'] = model_to_dict(suiteParams)
        data['suite_params']['auto_repair_seconds'] = GAME_SETTINGS['suite_damage']['auto_repair_seconds']
        data['suite_params']['repair_price'] = GAME_SETTINGS['suite_damage']['repair_price']
        data['suite_params']['free_jumps'] = GAME_SETTINGS['suite_damage']['free_jumps']

    return data


def getUsersListInfo(socNetIdsString):
    socNetIdsList = socNetIdsString.split(',')
    i = 0
    length = len(socNetIdsList)
    while i < length:
        socNetIdsList[i] = long(socNetIdsList[i])
        i += 1
    return {
        'users_list': {
            '_init': UserLogic().getUsersListInfo(socNetIdsList)
        }
    }


def registerUser(**kwargs):
    userLogic = UserLogic()
    listsLogic = ListsLogic()
    items = listsLogic.getItemsList()
    achievements = listsLogic.getAchievementsList()
    userInfo, inventory = userLogic.registerUser(**kwargs)
    suiteParams, itemInfo = getSuiteParams(userInfo['id'])
    return packRespFullUserInfo(userInfo, inventory, None, items, achievements, suiteParams)


def buyItem(userId, itemId, count, currency, socNet):
    userLogic = UserLogic()
    newItem, userInfo, achievementsList = userLogic.buyItem(userId, itemId, count, currency, socNet)

    key = '_add' if getattr(newItem, 'isNewRecord') else '_update'
    resp = {
        'inventory': {
            key: {
                newItem.id: model_to_dict(newItem)
            }
        },
        'user': model_to_dict(userInfo)
    }

    if achievementsList:
        resp['user_achievements'] = {
            '_add': {}
        }

        for achvRecord in achievementsList:
            resp['user_achievements']['_add'][achvRecord.id] = model_to_dict(achvRecord)

    return resp


def equipItem(userId, itemId):
    allList = UserLogic().equipItem(userId, itemId)
    suiteParams, itemInfo = getSuiteParams(userId)
    return {
        'inventory': {
            '_update': allList
        },
        'suite_params': model_to_dict(suiteParams)
    }


def exploreCapsule(explorerUserId, userId):
    if not str(userId).isdigit():
        raise NotCorrectError('Wrong user id')

    userInfo, inventoryRecord, coins = UserLogic().exploreCapsule(explorerUserId, userId)
    return {
        'user': model_to_dict(userInfo),
        'inventory': {
            '_add': {
                inventoryRecord.id: model_to_dict(inventoryRecord)
            }
        },
        'explore_coins': coins,
    }


def setPhotoAlbum(userId, photoAlbumId):
    if not str(photoAlbumId).isdigit():
        raise NotCorrectError('Wrong photo album id')
    userInfo = Users.objects.get(id=userId)
    userInfo.photo_album_id = photoAlbumId
    userInfo.save()
    UserLogic().clearUserInfoCache(userInfo.soc_net_id)
    return {
        'user': model_to_dict(userInfo)
    }
