from rest_api.logic.top import TopLogic

__author__ = 'Andre'


def getTop(topType):
    """getTop(type: string) -> dict"""
    usersList, inventoriesData = TopLogic().getTop(topType)
    answerData = []
    for userRecord in usersList:
        userId = userRecord['id']
        answerData.append({
            'user_info': userRecord,
            'inventory': {
                '_init': inventoriesData.get(userId, {})
            }
        })

    key = 'top_' + topType
    return {
        key: {
            '_init': answerData
        }
    }
