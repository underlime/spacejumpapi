from rest_api.logic import news
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.news import getNewsRead


__author__ = 'Andre'


def setNewsRead(userId, newsId, isShared):
    if newsId is None:
        raise NotCorrectError('News id is required')

    if not str(newsId).isdigit():
        raise NotCorrectError('Wrong news id format')
    newsId = long(newsId)

    if not str(isShared).isdigit():
        raise NotCorrectError('Wrong is shared format')
    isShared = bool(isShared)

    userInfo = news.setNewsRead(userId, newsId, isShared)

    data = {
        'news_read': {
            '_init': getNewsRead(userId)
        },
    }
    if userInfo is not None:
        data['user'] = {
            'coins': userInfo.coins,
        }
    return data
