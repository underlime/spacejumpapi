from django.conf import settings
from django.forms import model_to_dict
from rest_api.logic import actions


def getPrizes():
    return {
        'actions_prizes': settings.GAME_SETTINGS['soc_net']['vk_com']['actions_prizes']
    }


def rewardUser(socNetId, prizeType):
    userRecord = actions.rewardUser(socNetId, prizeType)
    userData = model_to_dict(userRecord)
    return {
        'user': {
            'coins': userData['coins'],
            'favorites_prize': userData['favorites_prize'],
            'join_group_prize': userData['join_group_prize'],
        }
    }