from rest_api.logic.lists import ListsLogic

__author__ = 'Andre'


def getItemsList():
    itemsList = ListsLogic().getItemsList()
    return {
        'items': {
            '_init': itemsList,
        }
    }


def getAchievementsList():
    achievementsList = ListsLogic().getAchievementsList()
    return {
        'achievements': {
            '_init': achievementsList,
        }
    }
