from django.forms import model_to_dict
from rest_api.logic.achievements import checkAchievementByCode

__author__ = 'Andre'


def checkAchievementResp(userId, code):
    achv, userInfo = checkAchievementByCode(userId, code)
    return packAchvData(achv, userInfo)


def packAchvData(achv=None, userInfo=None):
    resp = {}
    if achv:
        resp['user_achievements'] = {
            '_add': {
                achv.id: model_to_dict(achv)
            }
        }

    if userInfo:
        resp['user'] = model_to_dict(userInfo)

    return resp
