from rest_api.logic.trophies import TrophiesLogic

__author__ = 'Andre'


def getTrophiesCollectionsList():
    trophiesLogic = TrophiesLogic()
    collectionsList = trophiesLogic.getTrophiesCollectionsList()
    trophiesGroups = trophiesLogic.getTrophiesGroupByCollection()

    for id in collectionsList:
        collectionsList[id]['trophies_list'] = {
            '_init': trophiesGroups.get(id)
        }

    return {
        'trophies_collections': {
            '_init': collectionsList,
        }
    }


def getUsersTrophies(userId):
    answerData = getTrophiesCollectionsList()

    usersTrophies = TrophiesLogic().getUsersTrophies(userId)
    answerData.update({
        'users_trophies': {
            '_init': usersTrophies
        }
    })

    return answerData
