# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'GameSessionsLog', fields ['time']
        db.create_index('rest_api_gamesessionslog', ['time'])

        # Deleting field 'GameSessionsStatistics.mim_value'
        db.delete_column('rest_api_gamesessionsstatistics', 'mim_value')

        # Adding field 'GameSessionsStatistics.min_value'
        db.add_column('rest_api_gamesessionsstatistics', 'min_value',
                      self.gf('django.db.models.fields.IntegerField')(default=0, blank=True),
                      keep_default=False)


        # Changing field 'GameSessionsStatistics.avg_value'
        db.alter_column('rest_api_gamesessionsstatistics', 'avg_value', self.gf('django.db.models.fields.FloatField')())

    def backwards(self, orm):
        # Removing index on 'GameSessionsLog', fields ['time']
        db.delete_index('rest_api_gamesessionslog', ['time'])

        # Adding field 'GameSessionsStatistics.mim_value'
        db.add_column('rest_api_gamesessionsstatistics', 'mim_value',
                      self.gf('django.db.models.fields.IntegerField')(default=0, blank=True),
                      keep_default=False)

        # Deleting field 'GameSessionsStatistics.min_value'
        db.delete_column('rest_api_gamesessionsstatistics', 'min_value')


        # Changing field 'GameSessionsStatistics.avg_value'
        db.alter_column('rest_api_gamesessionsstatistics', 'avg_value', self.gf('django.db.models.fields.IntegerField')())

    models = {
        'rest_api.achievements': {
            'Meta': {'object_name': 'Achievements'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16', 'db_index': 'True'}),
            'description_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'})
        },
        'rest_api.achievementsstatistics': {
            'Meta': {'object_name': 'AchievementsStatistics'},
            'achievement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Achievements']", 'unique': 'True'}),
            'count': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'percents': ('django.db.models.fields.FloatField', [], {'default': '0', 'blank': 'True'})
        },
        'rest_api.bankstatistics': {
            'Meta': {'object_name': 'BankStatistics'},
            'coins': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'count_day': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'count_global': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'count_month': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'count_week': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'rest_api.gamesessions': {
            'Meta': {'object_name': 'GameSessions'},
            'difficulty': ('django.db.models.fields.CharField', [], {'default': "'medium'", 'max_length': '8'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'db_index': 'True'}),
            'trophy_flag': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Users']"})
        },
        'rest_api.gamesessionslog': {
            'Meta': {'object_name': 'GameSessionsLog'},
            'bonuse_seconds': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'booster_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'bronze_coins': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'coins': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'cola': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'difficulty': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'distance': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'gold_coins': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'heart_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_velocity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'pause_seconds': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'power_shield_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'score': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'seconds': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'session_time': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'shield_count': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'silver_coins': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(1988, 1, 1, 0, 0)', 'db_index': 'True'}),
            'trophy_found': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Users']"}),
            'win': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'rest_api.gamesessionsstatistics': {
            'Meta': {'object_name': 'GameSessionsStatistics'},
            'avg_value': ('django.db.models.fields.FloatField', [], {'default': '0.0', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_check': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(1988, 1, 1, 0, 0)', 'blank': 'True'}),
            'max_value': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'min_value': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'n_value': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '32', 'db_index': 'True'}),
            'sum_value': ('django.db.models.fields.BigIntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'rest_api.inventory': {
            'Meta': {'object_name': 'Inventory'},
            'count': ('django.db.models.fields.IntegerField', [], {'default': '1', 'blank': 'True'}),
            'equipped': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'hp': ('django.db.models.fields.IntegerField', [], {'default': '5', 'blank': 'True'}),
            'hp_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(1988, 1, 1, 0, 0)', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_suite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'users_item'", 'to': "orm['rest_api.Items']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'item_owner'", 'to': "orm['rest_api.Users']"})
        },
        'rest_api.items': {
            'Meta': {'object_name': 'Items'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'add_during_registration': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'code': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '32', 'unique': 'True', 'null': 'True', 'db_index': 'True'}),
            'description_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '512', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'max_hp': ('django.db.models.fields.IntegerField', [], {'default': '5', 'blank': 'True'}),
            'max_to_buy': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {}),
            'rubric': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'upgrade_price': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'})
        },
        'rest_api.shopstatistics': {
            'Meta': {'object_name': 'ShopStatistics'},
            'count_day': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'count_global': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'count_month': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'count_week': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Items']", 'unique': 'True'})
        },
        'rest_api.starsstatistics': {
            'Meta': {'object_name': 'StarsStatistics'},
            'count': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'percents': ('django.db.models.fields.FloatField', [], {'default': '0', 'blank': 'True'}),
            'stars': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'db_index': 'True'})
        },
        'rest_api.trophies': {
            'Meta': {'object_name': 'Trophies'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'trophies_collection': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['rest_api.TrophiesCollections']", 'null': 'True'})
        },
        'rest_api.trophiescollections': {
            'Meta': {'object_name': 'TrophiesCollections'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'})
        },
        'rest_api.users': {
            'Meta': {'object_name': 'Users'},
            'acceleration': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'coins': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'coins_improve_level': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'explored_capsules': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'favorites_prize': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'health': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'immortality': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'join_group_prize': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'jumps_count': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'landings_count': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'last_explore_guest_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(1988, 1, 1, 0, 0)', 'blank': 'True'}),
            'last_explore_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(1988, 1, 1, 0, 0)', 'blank': 'True'}),
            'max_score_day': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_global': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_month': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_week': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'place_in_top': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'place_in_top_time': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'referrer': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '32', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'soc_net_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'stars': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'summary_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'rest_api.usersachievements': {
            'Meta': {'object_name': 'UsersAchievements'},
            'achievement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Achievements']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Users']"})
        },
        'rest_api.userstrophies': {
            'Meta': {'object_name': 'UsersTrophies'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'trophy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Trophies']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Users']"})
        }
    }

    complete_apps = ['rest_api']