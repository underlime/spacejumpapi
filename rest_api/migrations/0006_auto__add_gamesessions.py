# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GameSessions'
        db.create_table('rest_api_gamesessions', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rest_api.Users'])),
            ('trophy_flag', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('rest_api', ['GameSessions'])


    def backwards(self, orm):
        # Deleting model 'GameSessions'
        db.delete_table('rest_api_gamesessions')


    models = {
        'rest_api.achievements': {
            'Meta': {'object_name': 'Achievements'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16', 'db_index': 'True'}),
            'criteria_current_coins': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'criteria_current_score': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'criteria_global_coins': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'criteria_global_score': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'})
        },
        'rest_api.gamesessions': {
            'Meta': {'object_name': 'GameSessions'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'trophy_flag': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Users']"})
        },
        'rest_api.inventory': {
            'Meta': {'object_name': 'Inventory'},
            'count': ('django.db.models.fields.IntegerField', [], {'default': '1', 'blank': 'True'}),
            'equipped': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'users_item'", 'to': "orm['rest_api.Items']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'item_owner'", 'to': "orm['rest_api.Users']"})
        },
        'rest_api.items': {
            'Meta': {'object_name': 'Items'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'add_during_registration': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'description_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'max_to_buy': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {}),
            'rubric': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'upgrade_price': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'})
        },
        'rest_api.trophies': {
            'Meta': {'object_name': 'Trophies'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'trophies_collection': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['rest_api.TrophiesCollections']", 'null': 'True'})
        },
        'rest_api.trophiescollections': {
            'Meta': {'object_name': 'TrophiesCollections'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'})
        },
        'rest_api.users': {
            'Meta': {'object_name': 'Users'},
            'acceleration': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'coins': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'coins_improve_level': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'favorites_prize': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'health': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'immortality': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'join_group_prize': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'jumps_count': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'landings_count': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'max_score_day': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_global': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_month': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_week': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'place_in_top': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'place_in_top_time': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'soc_net_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'stars': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'summary_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'rest_api.usersachievements': {
            'Meta': {'object_name': 'UsersAchievements'},
            'achievement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Achievements']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Users']"})
        },
        'rest_api.userstrophies': {
            'Meta': {'object_name': 'UsersTrophies'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'trophy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Trophies']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Users']"})
        }
    }

    complete_apps = ['rest_api']