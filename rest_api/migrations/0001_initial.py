# -*- coding: utf-8 -*-
from south.db import db
from south.v2 import SchemaMigration


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Users'
        db.create_table('rest_api_users', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('soc_net_id', self.gf('django.db.models.fields.IntegerField')(unique=True, db_index=True)),
            ('register_date', self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True)),
            ('max_score_global', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True, blank=True)),
            ('max_score_day', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True, blank=True)),
            ('day_score_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('max_score_week', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True, blank=True)),
            ('week_score_start_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('week_score_last_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('max_score_month', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True, blank=True)),
            ('month_score_start_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('month_score_last_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('jumps_count', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('landings_count', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('summary_time', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('coins', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('stars', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('health', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('acceleration', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('immortality', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('coins_improve_level', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('place_in_top', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True)),
            ('place_in_top_time', self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True)),
            ('favorites_prize', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('join_group_prize', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('rest_api', ['Users'])

        # Adding model 'Items'
        db.create_table('rest_api_items', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sort', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True)),
            ('rubric', self.gf('django.db.models.fields.CharField')(max_length=32, db_index=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=48, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(default='', max_length=48, null=True)),
            ('description_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=140, null=True, blank=True)),
            ('description_en', self.gf('django.db.models.fields.CharField')(default='', max_length=140, null=True, blank=True)),
            ('image', self.gf('django.db.models.fields.CharField')(default='', max_length=32)),
            ('symbol', self.gf('django.db.models.fields.CharField')(default='', max_length=32)),
            ('action', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('price', self.gf('django.db.models.fields.IntegerField')()),
            ('upgrade_price', self.gf('django.db.models.fields.CharField')(max_length=64, blank=True)),
            ('max_to_buy', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('add_during_registration', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
        ))
        db.send_create_signal('rest_api', ['Items'])

        # Adding model 'Inventory'
        db.create_table('rest_api_inventory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='item_owner', to=orm['rest_api.Users'])),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(related_name='users_item', to=orm['rest_api.Items'])),
            ('count', self.gf('django.db.models.fields.IntegerField')(default=1, blank=True)),
            ('equipped', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True, blank=True)),
        ))
        db.send_create_signal('rest_api', ['Inventory'])

        # Adding model 'Achievements'
        db.create_table('rest_api_achievements', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=16, db_index=True)),
            ('sort', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=48, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(default='', max_length=48, null=True)),
            ('description_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=140, null=True, blank=True)),
            ('description_en', self.gf('django.db.models.fields.CharField')(default='', max_length=140, null=True, blank=True)),
            ('image', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('criteria_current_score', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, db_index=True, blank=True)),
            ('criteria_global_score', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, db_index=True, blank=True)),
            ('criteria_current_coins', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, db_index=True, blank=True)),
            ('criteria_global_coins', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, db_index=True, blank=True)),
        ))
        db.send_create_signal('rest_api', ['Achievements'])

        # Adding model 'UsersAchievements'
        db.create_table('rest_api_usersachievements', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('achievement', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rest_api.Achievements'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rest_api.Users'])),
        ))
        db.send_create_signal('rest_api', ['UsersAchievements'])

        # Adding model 'TrophiesCollections'
        db.create_table('rest_api_trophiescollections', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=16, db_index=True)),
            ('sort', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=48, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(default='', max_length=48, null=True)),
        ))
        db.send_create_signal('rest_api', ['TrophiesCollections'])

        # Adding model 'Trophies'
        db.create_table('rest_api_trophies', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('trophies_collaction', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['rest_api.TrophiesCollections'])),
            ('sort', self.gf('django.db.models.fields.IntegerField')(default=0, db_index=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(default='', max_length=48, null=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(default='', max_length=48, null=True)),
            ('image', self.gf('django.db.models.fields.CharField')(default='', max_length=32)),
            ('symbol', self.gf('django.db.models.fields.CharField')(default='', max_length=32)),
        ))
        db.send_create_signal('rest_api', ['Trophies'])


    def backwards(self, orm):
        # Deleting model 'Users'
        db.delete_table('rest_api_users')

        # Deleting model 'Items'
        db.delete_table('rest_api_items')

        # Deleting model 'Inventory'
        db.delete_table('rest_api_inventory')

        # Deleting model 'Achievements'
        db.delete_table('rest_api_achievements')

        # Deleting model 'UsersAchievements'
        db.delete_table('rest_api_usersachievements')

        # Deleting model 'TrophiesCollections'
        db.delete_table('rest_api_trophiescollections')

        # Deleting model 'Trophies'
        db.delete_table('rest_api_trophies')


    models = {
        'rest_api.achievements': {
            'Meta': {'object_name': 'Achievements'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16', 'db_index': 'True'}),
            'criteria_current_coins': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'criteria_current_score': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'criteria_global_coins': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'criteria_global_score': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'})
        },
        'rest_api.inventory': {
            'Meta': {'object_name': 'Inventory'},
            'count': ('django.db.models.fields.IntegerField', [], {'default': '1', 'blank': 'True'}),
            'equipped': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'users_item'", 'to': "orm['rest_api.Items']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'item_owner'", 'to': "orm['rest_api.Users']"})
        },
        'rest_api.items': {
            'Meta': {'object_name': 'Items'},
            'action': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'add_during_registration': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'description_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '140', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'max_to_buy': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {}),
            'rubric': ('django.db.models.fields.CharField', [], {'max_length': '32', 'db_index': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'upgrade_price': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'})
        },
        'rest_api.trophies': {
            'Meta': {'object_name': 'Trophies'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'symbol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32'}),
            'trophies_collaction': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.TrophiesCollections']"})
        },
        'rest_api.trophiescollections': {
            'Meta': {'object_name': 'TrophiesCollections'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16', 'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_en': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '48', 'null': 'True'}),
            'sort': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'})
        },
        'rest_api.users': {
            'Meta': {'object_name': 'Users'},
            'acceleration': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'coins': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'coins_improve_level': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'day_score_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'favorites_prize': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'health': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'immortality': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'join_group_prize': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'jumps_count': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'landings_count': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'max_score_day': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_global': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_month': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'max_score_week': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True', 'blank': 'True'}),
            'month_score_last_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'month_score_start_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'place_in_top': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'place_in_top_time': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'register_date': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'soc_net_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'stars': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'summary_time': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'week_score_last_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'week_score_start_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'rest_api.usersachievements': {
            'Meta': {'object_name': 'UsersAchievements'},
            'achievement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Achievements']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['rest_api.Users']"})
        }
    }

    complete_apps = ['rest_api']