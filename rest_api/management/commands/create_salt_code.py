from django.core.management import BaseCommand

__author__ = 'Andre'


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not len(args):
            raise RuntimeError('Salt is required')

        saltValue = args[0]
        self.stdout.write('Salt value: ' + saltValue + '\n')

        valuesList = []
        for char in saltValue:
            value = ord(char) ^ 127
            valuesList.append(str(value))

        encryptedSalt = '[' + ','.join(valuesList) + ']'
        self.stdout.write('Encrypted salt: ' + encryptedSalt + '\n')
