from django.core import cache
from django.core.management import BaseCommand

__author__ = 'Andre'


class Command(BaseCommand):
    def handle(self, *args, **options):
        cache.cache.clear()
        self.stdout.write('Cache has cleared\n')
