# coding=utf-8
import time
import datetime
from django.core.management import BaseCommand
from rest_api.logic.socnet import SocNet
from rest_api.models import Inventory, Users
from spacejump.db_routers import DbRouter
from spacejump.settings import GAME_SETTINGS

__author__ = 'Andre'

MESSAGES_TABLE = {
    '1': u'Ваш скафандр восстановился. Вперед, навстречу приключениям!'
}

MAX_USER_IDS = 100
CALL_INTERVAL = 3600

#TODO: унифицировать сообщения
class Command(BaseCommand):
    def handle(self, *args, **options):
        if not len(args):
            self.stderr.write('Wrong message N\n')
            return

        message = MESSAGES_TABLE.get(args[0])
        DbRouter.currentDb = DbRouter.VK_COM

        timestamp = round(time.time() - GAME_SETTINGS['suite_damage']['auto_repair_seconds'])
        maxTime = datetime.datetime.utcfromtimestamp(timestamp)
        minTime = datetime.datetime.utcfromtimestamp(
            timestamp - CALL_INTERVAL - GAME_SETTINGS['suite_damage']['auto_repair_seconds'])

        restoredSuites = Inventory.objects.filter(hp__lt=5, hp_time__gt=minTime, hp_time__lt=maxTime, is_suite=True,
                                                  equipped=1)

        userIdsList = list((rec.user_id for rec in restoredSuites))
        usersList = Users.objects.filter(id__in=userIdsList)
        socNetIdsList = list((user.soc_net_id for user in usersList))

        socNet = SocNet.factory(None)
        while len(socNetIdsList):
            socNetIdsSlice = socNetIdsList[:MAX_USER_IDS]
            socNetIdsList = socNetIdsList[MAX_USER_IDS:]
            ans = socNet.sendNotification(socNetIdsSlice, message)
            self.stdout.write('Slice: ' + str(socNetIdsSlice) + '\n')
            self.stdout.write('Answer: ' + str(ans) + '\n')
