from django.core.management import BaseCommand
from rest_api import cache
from rest_api.constants import DATABASES_LIST
from rest_api.models import Users, Inventory, Items
from spacejump.db_routers import DbRouter


__author__ = 'Andre'


class Command(BaseCommand):
    def handle(self, *args, **options):
        for dbName in DATABASES_LIST:
            DbRouter.currentDb = dbName
            self.clearDayScore()

    def clearDayScore(self):
        Users.objects.all().update(max_score_day=0, place_in_top_time=None)
        self.stdout.write('Day score is 0 now ({0})\n'.format(DbRouter.currentDb))
        self._giveCrown()
        self.stdout.write('Crown is given ({0})\n'.format(DbRouter.currentDb))

    def _giveCrown(self):
        crownRecord = Items.objects.get(code='crown')
        currentCrownData = Inventory.objects.filter(item_id=crownRecord.id)

        topData = Users.objects.order_by('-max_score_global')
        if len(topData):
            topOne = topData[0]
            needNewRecord = False

            crownDataLength = len(currentCrownData)
            if crownDataLength and currentCrownData[0].user_id != topOne.id:
                userId = currentCrownData[0].user_id
                currentCrownData.delete()
                withoutHat = Inventory.objects.filter(user_id=userId, item__code='without_hat')
                if len(withoutHat):
                    withoutHat.update(equipped=1)
                cache.delDataFromCache('inventory', (userId,))
                needNewRecord = True
            elif not crownDataLength:
                needNewRecord = True

            if needNewRecord:
                Inventory.objects.filter(
                    user_id=topOne.id,
                    equipped=1,
                    item__action=Items.ACTION_HAT,
                ).update(equipped=0)

                newRecord = Inventory(
                    user_id=topOne.id,
                    item_id=crownRecord.id,
                    count=1,
                    equipped=1
                )
                newRecord.save()

            cache.delDataFromCache('inventory', (topOne.id,))

            cache.delDataFromCache('top', ('day',))
            cache.delDataFromCache('top', ('week',))
            cache.delDataFromCache('top', ('month',))
            cache.delDataFromCache('top', ('global',))
