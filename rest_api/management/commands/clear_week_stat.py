import datetime
from django.core.management import BaseCommand
from rest_api.constants import DATABASES_LIST
from rest_api.models import ShopStatistics, BankStatistics, UserNews
from spacejump.db_routers import DbRouter


__author__ = 'Andre'


class Command(BaseCommand):
    def handle(self, *args, **options):
        for dbName in DATABASES_LIST:
            DbRouter.currentDb = dbName
            ShopStatistics.objects.all().update(count_week=0)
            BankStatistics.objects.all().update(count_week=0)
            UserNews.objects.filter(expires_time__lte=datetime.datetime.utcnow()).delete()
            self.stdout.write('Week count is 0 now ({0})\n'.format(DbRouter.currentDb))
