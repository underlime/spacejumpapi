from django.core.management import BaseCommand
from rest_api.constants import DATABASES_LIST
from rest_api.models import ShopStatistics, BankStatistics
from spacejump.db_routers import DbRouter


__author__ = 'Andre'


class Command(BaseCommand):
    def handle(self, *args, **options):
        for dbName in DATABASES_LIST:
            DbRouter.currentDb = dbName
            ShopStatistics.objects.all().update(count_day=0)
            BankStatistics.objects.all().update(count_day=0)
            self.stdout.write('Day count is 0 now ({0})\n'.format(DbRouter.currentDb))
