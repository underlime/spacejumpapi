# coding=utf-8
import datetime
import time

from django.core.exceptions import ObjectDoesNotExist
from django.core.management import BaseCommand
from django.db.models import Count
from rest_api.constants import DATABASES_LIST

from rest_api.models import Users, StarsStatistics, UsersAchievements, AchievementsStatistics, GameSessions, GameSessionsLog, GameSessionsStatistics, ReferrersStatistics
from spacejump.db_routers import DbRouter


__author__ = 'Andre'

DIFFICULTIES_NAMES_TABLE = {
    1: u'Легкий',
    2: u'Нормальный',
    3: u'Сложный',
    4: u'Бесконечный',
}

FIELDS_LIST = (
    'session_time', 'win', 'bronze_coins', 'silver_coins', 'gold_coins', 'coins', 'score', 'trophy_found',
    'cola', 'distance', 'booster_count', 'power_shield_count', 'heart_count', 'shield_count', 'pause_seconds',
    'bonus_seconds', 'seconds', 'max_velocity'
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        for dbName in DATABASES_LIST:
            DbRouter.currentDb = dbName
            self._usersCount = Users.objects.count()
            self._nowTimestamp = time.mktime(datetime.datetime.utcnow().timetuple())
            self._collectStarsStat()
            self._collectAchievementsStat()
            self._collectReferrersStat()
            self._collectGameSessionsStat()
            self._clearOldGameSessions()

    def _collectStarsStat(self):
        starsData = Users.objects.values('stars').annotate(count=Count('stars'))

        for starsRecord in starsData:
            try:
                statRecord = StarsStatistics.objects.get(stars=starsRecord['stars'])
            except ObjectDoesNotExist:
                statRecord = StarsStatistics(stars=starsRecord['stars'])
            statRecord.count = starsRecord['count']
            statRecord.percents = round(starsRecord['count'] * 100.0 / self._usersCount, 2)
            statRecord.save()

        self.stdout.write('Stars statistics collected ({0})\n'.format(DbRouter.currentDb))

    def _collectAchievementsStat(self):
        achievementsData = UsersAchievements.objects.values('achievement_id').annotate(count=Count('achievement'))

        for achvRecord in achievementsData:
            try:
                statRecord = AchievementsStatistics.objects.get(achievement_id=achvRecord['achievement_id'])
            except ObjectDoesNotExist:
                statRecord = AchievementsStatistics(achievement_id=achvRecord['achievement_id'])
            statRecord.count = achvRecord['count']
            statRecord.percents = round(achvRecord['count'] * 100.0 / self._usersCount, 2)
            statRecord.save()

        self.stdout.write('Achievements statistics collected ({0})\n'.format(DbRouter.currentDb))

    def _collectReferrersStat(self):
        referrersData = Users.objects.values('referrer').annotate(count=Count('referrer'))

        for refRecord in referrersData:
            try:
                statRecord = ReferrersStatistics.objects.get(referrer=refRecord['referrer'])
            except ObjectDoesNotExist:
                statRecord = ReferrersStatistics(referrer=refRecord['referrer'])
            statRecord.count = refRecord['count']
            statRecord.percents = round(refRecord['count'] * 100.0 / self._usersCount, 2)
            statRecord.save()

        self.stdout.write('Referrers statistics collected ({0})\n'.format(DbRouter.currentDb))

    def _collectGameSessionsStat(self):
        self._initLogTable()
        try:
            borderRecord = GameSessionsStatistics.objects.get(name='session_time_1')
            borderDate = borderRecord.last_check.strftime('%Y-%m-%d %H:%M:%S')
        except ObjectDoesNotExist:
            borderDate = '1988-01-01 11:40:00'

        self._gameSessionsLogData = GameSessionsLog.objects.filter(time__gt=borderDate)
        if len(self._gameSessionsLogData):
            self._nValuesTable = {1: 0, 2: 0, 3: 0, 4: 0}
            for record in self._gameSessionsLogData:
                self._nValuesTable[record.difficulty] += 1
            for record in self._gameSessionsLogData:
                self._handleRecord(record)
            self._createGameSessionsStatistics()

        borderTime = self._nowTimestamp - 1209600
        borderDate = datetime.datetime.fromtimestamp(borderTime).strftime('%Y-%m-%d %H:%M:%S')
        GameSessionsLog.objects.filter(time__lt=borderDate).delete()

        self.stdout.write('Game statistics collected ({0})\n'.format(DbRouter.currentDb))

    def _initLogTable(self):
        self._gameLogParamsTable = {
            'difficulty': {1: 0, 2: 0, 3: 0, 4: 0},
            'session_time': self._createIntStatByDifficulty(),
            'win': self._createIntStatByDifficulty(),
            'bronze_coins': self._createIntStatByDifficulty(),
            'silver_coins': self._createIntStatByDifficulty(),
            'gold_coins': self._createIntStatByDifficulty(),
            'coins': self._createIntStatByDifficulty(),
            'score': self._createIntStatByDifficulty(),
            'trophy_found': self._createIntStatByDifficulty(),
            'cola': self._createIntStatByDifficulty(),
            'distance': self._createIntStatByDifficulty(),
            'booster_count': self._createIntStatByDifficulty(),
            'power_shield_count': self._createIntStatByDifficulty(),
            'heart_count': self._createIntStatByDifficulty(),
            'shield_count': self._createIntStatByDifficulty(),
            'pause_seconds': self._createIntStatByDifficulty(),
            'bonus_seconds': self._createIntStatByDifficulty(),
            'seconds': self._createIntStatByDifficulty(),
            'max_velocity': self._createIntStatByDifficulty(),
        }

    def _createIntStatDict(self):
        return {
            'n': 0,
            'sum': 0,
            'max': 0,
            'min': 0,
        }

    def _createIntStatByDifficulty(self):
        return {
            1: self._createIntStatDict(),
            2: self._createIntStatDict(),
            3: self._createIntStatDict(),
            4: self._createIntStatDict(),
        }

    def _handleRecord(self, record):
        self._gameLogParamsTable['difficulty'][record.difficulty] += 1
        for fieldName in FIELDS_LIST:
            self._handleRecordByDifficulty(record, fieldName)

    def _handleRecordByDifficulty(self, record, diefldName):
        value = getattr(record, diefldName)
        self._gameLogParamsTable[diefldName][record.difficulty]['n'] += 1
        self._gameLogParamsTable[diefldName][record.difficulty]['sum'] += value
        if value > self._gameLogParamsTable[diefldName][record.difficulty]['max']:
            self._gameLogParamsTable[diefldName][record.difficulty]['max'] = value
        if value < self._gameLogParamsTable[diefldName][record.difficulty]['min'] \
                or self._gameLogParamsTable[diefldName][record.difficulty]['min'] == 0:
            self._gameLogParamsTable[diefldName][record.difficulty]['min'] = value

    def _createGameSessionsStatistics(self):
        diffRange = range(1, 5)
        for difficulty in diffRange:
            self._handleDifficultyStat(difficulty)
            for fieldName in FIELDS_LIST:
                self._handleCommonFieldStat(fieldName, difficulty)

    def _handleDifficultyStat(self, difficulty):
        diffRecord, fullFieldName = self._createStatRecord('difficulty', difficulty)
        diffRecord.name = fullFieldName
        diffRecord.description = GameSessionsLog._meta.get_field_by_name('difficulty')[0].verbose_name + \
            u' ({0})'.format(DIFFICULTIES_NAMES_TABLE.get(difficulty, u'ХЗ'))
        diffRecord.sort = difficulty
        diffRecord.sum_value += self._gameLogParamsTable['difficulty'][difficulty]
        diffRecord.n_value += self._nValuesTable[difficulty]
        diffRecord.last_check = datetime.datetime.fromtimestamp(self._nowTimestamp).strftime('%Y-%m-%d %H:%M:%S')
        diffRecord.save()

    def _handleCommonFieldStat(self, fieldName, difficulty):
        record, fullFieldName = self._createStatRecord(fieldName, difficulty)
        record.name = fullFieldName
        record.description = GameSessionsLog._meta.get_field_by_name(fieldName)[0].verbose_name + \
            u' ({0})'.format(DIFFICULTIES_NAMES_TABLE.get(difficulty, u'ХЗ'))
        record.sort = FIELDS_LIST.index(fieldName) + 5
        record.sum_value += self._gameLogParamsTable[fieldName][difficulty]['sum']
        record.n_value += self._nValuesTable[difficulty]
        if self._gameLogParamsTable[fieldName][difficulty]['min'] < record.min_value or record.min_value == 0:
            record.min_value = self._gameLogParamsTable[fieldName][difficulty]['min']
        if self._gameLogParamsTable[fieldName][difficulty]['max'] > record.max_value:
            record.max_value = self._gameLogParamsTable[fieldName][difficulty]['max']
        if record.n_value:
            record.avg_value = float(record.sum_value) / record.n_value
        else:
            record.avg_value = 0
        record.last_check = datetime.datetime.fromtimestamp(self._nowTimestamp).strftime('%Y-%m-%d %H:%M:%S')
        record.save()

    def _createStatRecord(self, fieldName, difficulty):
        fullFieldName = fieldName + '_' + str(difficulty)
        try:
            statRecord = GameSessionsStatistics.objects.get(name=fullFieldName)
        except ObjectDoesNotExist:
            statRecord = GameSessionsStatistics(name=fullFieldName)
        return statRecord, fullFieldName

    def _clearOldGameSessions(self):
        borderTimestamp = self._nowTimestamp - 172800
        borderDateTime = datetime.datetime.fromtimestamp(borderTimestamp).strftime('%Y-%m-%d %H:%M:%S')
        GameSessions.objects.filter(start_time__lt=borderDateTime).delete()
        self.stdout.write('Game sessions later then {0} has deleted ({1})\n'.format(borderDateTime, DbRouter.currentDb))
