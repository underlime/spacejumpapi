from django.core.management import BaseCommand
from rest_api.constants import DATABASES_LIST
from rest_api.models import Users
from spacejump.db_routers import DbRouter


__author__ = 'Andre'


class Command(BaseCommand):
    def handle(self, *args, **options):
        for dbName in DATABASES_LIST:
            DbRouter.currentDb = dbName
            Users.objects.all().update(max_score_week=0)
            self.stdout.write('Week score is 0 now ({0})\n'.format(DbRouter.currentDb))
