from django.core.management import BaseCommand
from django.forms import model_to_dict
from rest_api.models import Trophies, Achievements, Items, TrophiesCollections

__author__ = 'Andre'


MODELS_LIST = (Achievements, Items, TrophiesCollections)
DATABASES_LIST = ('facebook_com', 'odnoklassniki_ru', 'my_mail_ru')


class Command(BaseCommand):
    def handle(self, *args, **options):
        for ModelClass in MODELS_LIST:
            self._syncModel(ModelClass)
        self._syncTrophies()

    def _syncModel(self, ModelClass):
        ethalonData = ModelClass.objects.using('default').all()
        ethalonTable = {}
        for record in ethalonData:
            ethalonTable[record.code] = record

        for dbName in DATABASES_LIST:
            otherData = ModelClass.objects.using(dbName).all()
            otherTable = {}
            for record in otherData:
                otherTable[record.code] = record

            for recordCode in ethalonTable:
                data = model_to_dict(ethalonTable[recordCode])
                otherRecord = otherTable.get(recordCode)
                if otherRecord is None:
                    otherRecord = ModelClass(**data)
                else:
                    for (key, value) in data.items():
                        if key != 'id':
                            setattr(otherRecord, key, value)
                otherRecord.save(using=dbName)

            self.stdout.write('{0} syncronized in {1}\n'.format(ModelClass.__name__, dbName))

    def _syncTrophies(self):
        ethalonData = Trophies.objects.using('default').all()
        ethalonTable = {}
        for record in ethalonData:
            ethalonTable[record.code] = record

        for dbName in DATABASES_LIST:
            otherData = Trophies.objects.using(dbName).all()
            otherCodesList = []
            for record in otherData:
                otherCodesList.append(record.code)

            for recordCode in ethalonTable:
                try:
                    otherCodesList.index(recordCode)
                except ValueError:
                    data = model_to_dict(ethalonTable[recordCode])
                    collectionId = data.pop('trophies_collection', None)
                    data['trophies_collection_id'] = collectionId
                    newRecord = Trophies(**data)
                    newRecord.save(using=dbName)
            self.stdout.write('Trophies syncronized in {0}\n'.format(dbName))
