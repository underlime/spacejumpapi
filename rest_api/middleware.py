from django.conf import settings
from spacejump.db_routers import DbRouter
from spacejump.settings_control_sets import CURRENT_CONTROL_SET


__author__ = 'Andre'


class RouteDb(object):
    def process_request(self, request):
        pathInfo = request.META.get('PATH_INFO', '')
        httpHost = request.META.get('HTTP_HOST', '')

        if pathInfo.find('/api/facebook.com') == 0 or httpHost.find('fb-admin') == 0:
            DbRouter.currentDb = DbRouter.FACEBOOK_COM
        elif pathInfo.find('/api/vk.com') == 0 or httpHost.find('vk-admin') == 0:
            DbRouter.currentDb = DbRouter.VK_COM
        elif pathInfo.find('/api/odnoklassniki.ru') == 0 or httpHost.find('ok-admin') == 0:
            DbRouter.currentDb = DbRouter.ODNOKLASSNIKI_RU
        elif pathInfo.find('/api/my.mail.ru') == 0 or httpHost.find('mm-admin') == 0:
            DbRouter.currentDb = DbRouter.MY_MAIL_RU
        elif CURRENT_CONTROL_SET == 'localhost':
            DbRouter.currentDb = DbRouter.VK_COM
        elif not settings.DEBUG:
            raise RuntimeError('Wrong database')
