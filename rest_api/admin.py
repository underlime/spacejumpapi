__author__ = 'Andre'

from django.contrib import admin
from models import *


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'soc_net_id', 'register_date')
    date_hierarchy = 'register_date'

admin.site.register(Users, UserAdmin)


class ItemsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name_ru', 'name_en', 'rubric', 'action', 'price', 'gems_price', 'sort')
    list_filter = ('rubric', 'action')
    ordering = ('sort',)

admin.site.register(Items, ItemsAdmin)


class InventoryAdmin(admin.ModelAdmin):
    list_display = ('user', 'item', 'count', 'equipped')
    list_filter = ('item',)
    raw_id_fields = ('user', 'item')

admin.site.register(Inventory, InventoryAdmin)


class AchievementsAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name_ru', 'name_en', 'sort',)
    ordering = ('sort',)

admin.site.register(Achievements, AchievementsAdmin)


class UsersAchievementsAdmin(admin.ModelAdmin):
    list_display = ('user', 'achievement')
    list_filter = ('achievement',)
    raw_id_fields = ('user', 'achievement')

admin.site.register(UsersAchievements, UsersAchievementsAdmin)


class TrophiesCollectionsAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name_ru', 'name_en', 'sort',)
    ordering = ('sort',)

admin.site.register(TrophiesCollections, AchievementsAdmin)


class TrophiesAdmin(admin.ModelAdmin):
    list_display = ('name_ru', 'trophies_collection', 'sort')
    list_filter = ('trophies_collection',)
    raw_id_fields = ('trophies_collection',)
    ordering = ('sort',)

admin.site.register(Trophies, TrophiesAdmin)


class UsersTrophiesAdmin(admin.ModelAdmin):
    list_display = ('user', 'trophy')
    list_filter = ('trophy',)
    raw_id_fields = ('user', 'trophy')

admin.site.register(UsersTrophies, UsersTrophiesAdmin)


class ShopStatisticsAdmin(admin.ModelAdmin):
    list_display = ('item', 'count_global', 'count_day', 'count_week', 'count_month')
    list_filter = ('item__action', 'item__rubric',)
    raw_id_fields = ('item',)
    ordering = ('-count_day',)

admin.site.register(ShopStatistics, ShopStatisticsAdmin)


class BankStatisticsAdmin(admin.ModelAdmin):
    list_display = ('coins', 'count_global', 'count_day', 'count_week', 'count_month')
    ordering = ('-count_day',)

admin.site.register(BankStatistics, BankStatisticsAdmin)


class StarsStatisticsAdmin(admin.ModelAdmin):
    list_display = ('stars', 'count', 'percents')
    ordering = ('-count',)

admin.site.register(StarsStatistics, StarsStatisticsAdmin)


class AchievementsStatisticsAdmin(admin.ModelAdmin):
    list_display = ('achievement', 'count', 'percents')
    ordering = ('-count',)

admin.site.register(AchievementsStatistics, AchievementsStatisticsAdmin)


class ReferrersStatisticsAdmin(admin.ModelAdmin):
    list_display = ('referrer', 'count', 'percents')
    ordering = ('-count',)

admin.site.register(ReferrersStatistics, ReferrersStatisticsAdmin)


class GameSessionsStatisticsAdmin(admin.ModelAdmin):
    list_display = ('description', 'avg_value', 'min_value', 'max_value', 'n_value', 'last_check')
    ordering = ('sort', 'name')
admin.site.register(GameSessionsStatistics, GameSessionsStatisticsAdmin)


class UserNewsAdmin(admin.ModelAdmin):
    list_display = ('user', 'news_type')
    list_filter = ('news_type',)
    raw_id_fields = ('user',)
admin.site.register(UserNews, UserNewsAdmin)
