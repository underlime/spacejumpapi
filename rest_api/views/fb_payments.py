# coding=utf-8
import base64
import hashlib
import hmac
import json
import re

from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from rest_api.logic.user import UserLogic
from rest_api.logic.exceptions import NotCorrectError, SocNetParamsError
from rest_api.models import Users, BankStatistics


__author__ = 'Andre'


def getCoinsPrice(count):
    count = int(count)

    pricesData = settings.GAME_SETTINGS['soc_net']['facebook_com']['coins_prices']
    price = None
    for currentData in pricesData:
        if currentData['coins'] == count:
            price = currentData['price']
            break

    return price


def getGemsPrice(count):
    count = int(count)

    pricesData = settings.GAME_SETTINGS['soc_net']['facebook_com']['gems_prices']
    price = None
    for currentData in pricesData:
        if currentData['gems'] == count:
            price = currentData['price']
            break

    return price


class FbPaymentsView(View):
    @transaction.commit_manually
    def post(self, request):
        self._request = request
        self._httpStatus = 200
        success = False
        try:
            self._setInput()
            self._makeAction()
            success = True
        except (NotCorrectError, SocNetParamsError), e:
            self._setErrorAnswer(e.message)
        finally:
            if success:
                transaction.commit()
            else:
                transaction.rollback()

        return HttpResponse(
            json.dumps(self._actionResult, ensure_ascii=False, cls=DjangoJSONEncoder),
            mimetype="application/json; charset=UTF-8",
            status=self._httpStatus,
        )

    def _setInput(self):
        self._signedRequest = self._request.POST.get('signed_request')
        self._method = self._request.POST.get('method')
        if not self._signedRequest:
            raise NotCorrectError('Signed request required')

        encodedSig, payload = self._signedRequest.split('.', 2)

        strPayload = str(payload)
        jsonData = base64.urlsafe_b64decode(strPayload+'==')
        try:
            self._input = json.loads(jsonData)
            assert(self._input['algorithm'].upper() == 'HMAC-SHA256')
        except (ValueError, AssertionError):
            raise SocNetParamsError('Wrong signed request')

        strEncodedSig = str(encodedSig)
        sig = base64.urlsafe_b64decode(strEncodedSig+'==')

        secret = settings.GAME_SETTINGS['soc_net']['facebook_com']['api_secret']
        rightSig = hmac.new(secret, msg=strPayload, digestmod=hashlib.sha256).digest()

        if sig != rightSig:
            raise SocNetParamsError('Wrong sig')

    def _makeAction(self):
        if self._method == 'payments_get_items':
            self._getItem()
        elif self._method == 'payments_status_update':
            self._handleOrderRequest()
        else:
            raise NotCorrectError('Action is invalid')

    def _getItem(self):
        itemId = self._getItemId()
        currency, count = self._getItemInfo(itemId)
        itemResponse = self._createItemResponse(itemId, currency, count)
        self._actionResult = {
            'content': [itemResponse],
            'method': 'payments_get_items'
        }

    def _getItemId(self):
        creditsData = self._input.get('credits', {})
        orderJsonData = creditsData.get('order_info')
        try:
            orderInfo = json.loads(orderJsonData)
        except ValueError:
            raise NotCorrectError('Wrong order data')
        return orderInfo.get('item_id')

    def _getItemInfo(self, itemId):
        matches = re.findall(r'^((?:coins)|(?:gems))_(\d+)$', itemId)
        if not matches or len(matches[0]) < 2:
            raise NotCorrectError('Item is invalid')
        return unicode(matches[0][0]), int(matches[0][1])

    def _createItemResponse(self, itemId, currency, count):
        if currency == 'coins':
            picture = settings.GAME_SETTINGS['soc_net']['facebook_com']['coins_picture']
        else:
            picture = settings.GAME_SETTINGS['soc_net']['facebook_com']['gems_picture']

        return {
            'item_id': itemId,
            'title': self._getTitle(currency, count),
            'description': 'Buy some coins for space diving',
            'price': self._getVotesPrice(currency, count),
            'image_url': picture,
        }

    def _getTitle(self, currency, count):
        if currency == 'coins':
            currencyName = u'$'
        else:
            currencyName = u'G'
        return u'{0} {1}'.format(count, currencyName)

    def _getVotesPrice(self, currency, count):
        if currency == 'coins':
            price = getCoinsPrice(count)
        else:
            price = getGemsPrice(count)

        if price is None:
            err = NotCorrectError('Wrong count')
            raise err
        return price

    def _setErrorAnswer(self, message):
        self._httpStatus = 400
        self._actionResult = {
            'error': message,
        }

    def _handleOrderRequest(self):
        self._extractOrderInfo()
        if self._currentOrderStatus == 'placed':
            self._makeAddMoney()
        elif self._currentOrderStatus == 'settled':
            self._settleOrder()
        else:
            self._actionResult = {}

    def _extractOrderInfo(self):
        creditsData = self._input.get('credits', {})
        orderJsonData = creditsData.get('order_details')
        try:
            self._orderDetails = json.loads(orderJsonData)
            itemsData = self._orderDetails.get('items')
            if not (isinstance(itemsData, list) and len(itemsData)):
                raise NotCorrectError('Wrong items data')
            itemInfo = itemsData[0]
        except (ValueError, NotCorrectError):
            raise NotCorrectError('Wrong order data')
        self._itemInfo = itemInfo
        self._earnedCurrencyOrder = self._orderDetails.get('modified')
        self._currentOrderStatus = self._orderDetails.get('status')

    def _settleOrder(self):
        self._actionResult = {
            'content': {
                'status': 'settled',
                'order_id': self._orderDetails['order_id'],
            },
            'method': 'payments_status_update',
        }

    def _makeAddMoney(self):
        self._getUser()
        self._addMoney()
        self._actionResult = {
            'content': {
                'status': 'settled',
                'order_id': self._orderDetails['order_id'],
            },
            'method': 'payments_status_update',
        }

    def _getUser(self):
        userId = self._orderDetails.get('receiver')
        if not userId:
            raise NotCorrectError('User id are not passed')
        try:
            self._user = Users.objects.get(soc_net_id=userId)
        except ObjectDoesNotExist:
            raise NotCorrectError('User does not exists')

    def _addMoney(self):
        itemId = self._itemInfo['item_id']
        currency, count = self._getItemInfo(itemId)
        if currency == 'coins':
            self._user.coins += count
        else:
            self._user.gems += count
        self._user.save()
        UserLogic().clearUserInfoCache(self._user.soc_net_id)
        self._logMoney(currency, count)

    def _logMoney(self, currency, count):
        try:
            statRecord = BankStatistics.objects.get(coins=count)
        except ObjectDoesNotExist:
            statRecord = BankStatistics(coins=count)
        statRecord.count_global += 1
        statRecord.count_day += 1
        statRecord.count_week += 1
        statRecord.count_month += 1
        statRecord.save()
