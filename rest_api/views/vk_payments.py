# coding=utf-8
import hashlib
import json
import re

from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from rest_api.logic.user import UserLogic
from rest_api.logic.exceptions import NotCorrectError
from rest_api.models import Users, BankStatistics


__author__ = 'Andre'

ERROR_GENERAL = 1
ERROR_TEMPORARY = 2
ERROR_SIGNATURE = 10
ERROR_WRONG_REQUEST = 11
ERROR_WRONG_ITEM = 20
ERROR_ITEM_IS_NO_MORE = 21
ERROR_USER_NOT_FOUND = 22
ERROR_CUSTOM = 100


def getCoinsPrice(count):
    count = int(count)

    pricesData = settings.GAME_SETTINGS['soc_net']['vk_com']['coins_prices']
    price = None
    for currentData in pricesData:
        if currentData['coins'] == count:
            price = currentData['price']
            break

    return price


def getGemsPrice(count):
    count = int(count)

    pricesData = settings.GAME_SETTINGS['soc_net']['vk_com']['gems_prices']
    price = None
    for currentData in pricesData:
        if currentData['gems'] == count:
            price = currentData['price']
            break

    return price


class VkPaymentsView(View):
    @transaction.commit_manually
    def post(self, request):
        self._request = request
        success = False
        try:
            self._setInput()
            self._checkSignature()
            self._makeAction()
            success = True
        except NotCorrectError, e:
            code = getattr(e, 'code', ERROR_CUSTOM)
            self._setErrorAnswer(e.message, code)
        finally:
            if success:
                transaction.commit()
            else:
                transaction.rollback()

        return HttpResponse(
            json.dumps(self._actionResult, ensure_ascii=False, cls=DjangoJSONEncoder),
            mimetype="application/json; charset=UTF-8"
        )

    def _setInput(self):
        self._inputKeysList = sorted(self._request.POST.iterkeys())
        self._input = self._request.POST

    def _checkSignature(self):
        passedSig = self._input.get('sig')
        if not passedSig:
            err = NotCorrectError('Signature is not passed')
            err.code = ERROR_SIGNATURE
            raise err

        baseString = u''
        for key in self._inputKeysList:
            if key != 'sig':
                newPart = key + u'=' + self._input[key]
                baseString += newPart

        baseString = baseString.encode('utf-8')
        apiSecret = settings.GAME_SETTINGS['soc_net']['vk_com']['api_secret']
        trueSig = hashlib.md5(baseString + apiSecret).hexdigest()
        if passedSig != trueSig:
            err = NotCorrectError('Signature is invalid')
            err.code = ERROR_SIGNATURE
            raise err

    def _makeAction(self):
        actionType = self._input.get('notification_type')
        if actionType in ('get_item', 'get_item_test'):
            self._getItem()
        elif actionType in ('order_status_change', 'order_status_change_test'):
            self._depositMoney()
        else:
            err = NotCorrectError('Action is invalid')
            err.code = ERROR_GENERAL
            raise err

    def _getItem(self):
        itemId = self._getItemId()
        currency, count = self._getItemInfo(itemId)
        self._actionResult = {
            'response': self._createItemResponse(itemId, currency, count)
        }

    def _getItemId(self):
        itemId = self._input.get('item')
        if not itemId:
            itemId = self._input.get('item_id')
        if not itemId:
            err = NotCorrectError('Item is not passed')
            err.code = ERROR_WRONG_REQUEST
            raise err
        return itemId

    def _getItemInfo(self, itemId):
        matches = re.findall(r'^((?:coins)|(?:gems))_(\d+)$', itemId)
        if not matches or len(matches[0]) < 2:
            err = NotCorrectError('Item is invalid')
            err.code = ERROR_WRONG_ITEM
            raise err
        return unicode(matches[0][0]), int(matches[0][1])

    def _createItemResponse(self, itemId, currency, count):
        if currency == 'coins':
            picture = settings.GAME_SETTINGS['soc_net']['vk_com']['coins_picture']
        else:
            picture = settings.GAME_SETTINGS['soc_net']['vk_com']['gems_picture']

        return {
            'item_id': itemId,
            'title': self._getTitle(currency, count),
            'photo_url': picture,
            'price': self._getVotesPrice(currency, count),
        }

    def _getTitle(self, currency, count):
        if currency == 'coins':
            currencyName = u'$'
        else:
            mod10 = count%10
            mod100 = count%100
            not11 = (mod100 <= 10 or mod100 >= 20)
            if 2 <= mod10 <= 4 and not11:
                currencyName = u'кристалла'
            elif (not not11) or (mod10 == 0) or (not11 and mod10 > 4):
                currencyName = u'кристаллов'
            else:
                currencyName = u'кристалл'
        return u'{0} {1}'.format(count, currencyName)

    def _getVotesPrice(self, currency, count):
        if currency == 'coins':
            price = getCoinsPrice(count)
        else:
            price = getGemsPrice(count)

        if price is None:
            err = NotCorrectError('Wrong count')
            err.code = ERROR_CUSTOM
            raise err
        return price

    def _setErrorAnswer(self, message, code):
        self._actionResult = {
            'error_code': code,
            'error_msg': message,
            'critical': 1,
        }

    def _depositMoney(self):
        self._checkUser()
        self._addMoney()
        self._actionResult = {
            'response': {
                'order_id': self._input.get('order_id')
            }
        }

    def _checkUser(self):
        userId = self._input.get('user_id')
        receiverId = self._input.get('receiver_id')

        if not (userId and receiverId):
            err = NotCorrectError('User and receiver id are not passed')
            err.code = ERROR_WRONG_REQUEST
            raise err

        if userId != receiverId:
            err = NotCorrectError('Receiver id is invalid')
            err.code = ERROR_WRONG_REQUEST
            raise err

        try:
            self._user = Users.objects.get(soc_net_id=receiverId)
        except ObjectDoesNotExist:
            err = NotCorrectError('User does not exists')
            err.code = ERROR_USER_NOT_FOUND
            raise err

    def _addMoney(self):
        itemId = self._getItemId()
        currency, count = self._getItemInfo(itemId)
        if currency == 'coins':
            self._user.coins += count
        else:
            self._user.gems += count
        self._user.save()
        UserLogic().clearUserInfoCache(self._user.soc_net_id)
        self._logMoney(currency, count)

    def _logMoney(self, currency, count):
        try:
            statRecord = BankStatistics.objects.get(coins=count)
        except ObjectDoesNotExist:
            statRecord = BankStatistics(coins=count)
        statRecord.count_global += 1
        statRecord.count_day += 1
        statRecord.count_week += 1
        statRecord.count_month += 1
        statRecord.save()