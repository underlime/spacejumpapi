# coding=utf-8
import hashlib
import json

from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from rest_api import models, cache
from spacejump import settings
from rest_api.logic.user import UserLogic
from rest_api.logic.exceptions import NotCorrectError, SocNetAuthError


__author__ = 'Andre'

ERROR_GENERAL = 700
ERROR_USER_NOT_FOUND = 701
ERROR_SERVICE_NOT_FOUND = 702
ERROR_PRICE = 703


def getGemsPrice(count):
    count = int(count)

    pricesData = settings.GAME_SETTINGS['soc_net']['my_mail_ru']['gems_prices']
    price = None
    for currentData in pricesData:
        if currentData['gems'] == count:
            price = currentData['price']
            break

    return price


class MmPaymentsView(View):
    @transaction.commit_manually
    def get(self, request, *args, **kwargs):
        self._request = request

        errorCode = self._run()
        if errorCode == 0:
            transaction.commit()
            data = {
                'status': '1',
                'duplicate': self._duplicate
            }
        else:
            transaction.rollback()
            data = {
                'status': '2',
                'error_code': errorCode
            }

        if settings.DEBUG:
            print 'Payment response:\n', data
        response = json.dumps(data, ensure_ascii=False, cls=DjangoJSONEncoder),
        return HttpResponse(response, mimetype='application/json; charset=UTF-8')

    def _run(self):
        errorCode = 0
        self._duplicate = False
        try:
            self._setParams()
            self._checkSig()
            self._duplicate = self._isTransactionDuplicate()
            if not self._duplicate:
                self._addMoney()
        except (NotCorrectError, SocNetAuthError) as e:
            errorCode = ERROR_SERVICE_NOT_FOUND
        except ObjectDoesNotExist as e:
            errorCode = ERROR_USER_NOT_FOUND
        return errorCode

    def _setParams(self):
        self._uid = self._request.GET.get('uid')
        self._transactionId = self._request.GET.get('transaction_id')
        self._serviceId = int(self._request.GET.get('service_id', '0'))
        self._sig = self._request.GET.get('sig')
        self._price = int(self._request.GET.get('mailiki_price', '0'))
        self._profit = float(self._request.GET.get('profit', '0'))
        self._duplicate = False

        if not (self._uid and self._transactionId and self._serviceId and self._sig and self._price):
            raise NotCorrectError('No params')

    def _checkSig(self):
        params = self._request.GET
        baseString = ''
        for key in sorted(params.iterkeys()):
            if key != 'sig':
                baseString += str(key) + '=' + str(params[key])
        baseString += settings.GAME_SETTINGS['soc_net']['my_mail_ru']['api_secret']
        rightSig = hashlib.md5(baseString).hexdigest()

        if self._sig != rightSig:
            raise SocNetAuthError('Wrong signature')

    def _isTransactionDuplicate(self):
        key = self._getTransactionCheckKey()
        return bool(cache.getItem(key, False))

    def _getTransactionCheckKey(self):
        return 'was_payment_transaction:' + self._transactionId

    def _addMoney(self):
        count = long(self._serviceId)
        price = getGemsPrice(count)
        if price != long(self._price):
            raise NotCorrectError('Wrong price')

        userModel = models.Users.objects.get(soc_net_id=self._uid)
        userModel.gems += count
        userModel.save()
        UserLogic().clearUserInfoCache(self._uid)

        key = self._getTransactionCheckKey()
        cache.setItem(key, True, 108000)
