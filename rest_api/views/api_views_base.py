# coding=utf-8
import json

from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http import HttpResponse
from django.views.generic.base import View
from django.core.serializers.json import DjangoJSONEncoder

from rest_api.logic.exceptions import NotCorrectError, SocNetParamsError, SocNetAuthError
from rest_api.logic.socnet import SocNet
from rest_api.logic.user import UserLogic
from rest_api.response.achievements import checkAchievementResp


__author__ = 'Andre'


class ApiView(View):
    def __init__(self, **kwargs):
        self._socNet = None
        self._needCheckAuthParams = True
        self._needGetUserInfo = True
        super(ApiView, self).__init__(**kwargs)

    @transaction.commit_manually
    def dispatch(self, request, *args, **kwargs):
        try:
            self.__userDataInit(request)
            dispatchResult = super(ApiView, self).dispatch(request, *args, **kwargs)
            transaction.commit()
            return dispatchResult
        except (AttributeError, SocNetParamsError, NotCorrectError, ObjectDoesNotExist, ValueError) as e:
            transaction.rollback()
            return self._renderError(code=400, message=unicode(e))
        except SocNetAuthError as e:
            transaction.rollback()
            return self._renderError(code=403, message=unicode(e))
        except BaseException:
            transaction.rollback()
            raise

    def __userDataInit(self, request):
        self._socNet = SocNet.factory(request)
        if self._needCheckAuthParams:
            self._socNet.checkAuthParams()
            self.__checkToken(request)
            if self._needGetUserInfo:
                self.__userInfoInit()

    def __checkToken(self, request):
        token = request.POST.get('token')
        rightToken = self._socNet.makeToken()
        if token != rightToken:
            raise SocNetAuthError('Token is invalid')

    def __userInfoInit(self):
        socNetId = self._socNet.getSocNetId()
        self._userInfo = UserLogic().getUserInfo(socNetId)

    def http_method_not_allowed(self, request, *args, **kwargs):
        return self._renderError(code=405, message='Method not allowed')

    def _renderError(self, code=500, message=''):
        data = {
            'error': {
                'code': code,
                'message': message,
            }
        }
        return self._renderJsonResponse(data)

    def _renderJsonResponse(self, respData):
        return HttpResponse(
            json.dumps(respData, ensure_ascii=False, cls=DjangoJSONEncoder),
            mimetype="application/json; charset=UTF-8"
        )


class AchievementsView(ApiView):
    def __init__(self, **kwargs):
        self._code = None
        super(AchievementsView, self).__init__(**kwargs)

    def post(self, request, **kwargs):
        userId = self._userInfo['id']
        result = checkAchievementResp(userId, self._code)
        if 'user' in result:
            userInfo = result['user']
            self._socNet.setUserLevel(userInfo['soc_net_id'], userInfo['stars'])
        return self._renderJsonResponse(result)
