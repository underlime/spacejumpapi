# coding=utf-8
from httplib import HTTPConnection
import json
import re
from urllib import urlencode
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from rest_api.logic.socnet import VkComSocNet
from rest_api.views.web_views_base import WebView, ViewForAdmin

__author__ = 'Andre'


class TestingView(ViewForAdmin, WebView):
    METHODS_LIST = {
        'user/get.info': {
            'soc_net_id': '',
            'base_info_only': '0',
        },
        'user/get.list.info': {
            'soc_net_ids_list': '',
        },
        'user/get.coins': {},
        'user/register': {
            'referrer': 'test',
        },
        'user/set.photo.album': {
            'photo_album_id': '',
        },
        'user/give.friends.bonuses': {
            'bonus_type': 'gem',
            'locale': 'en',
        },
        'items/get.market.list': {},
        'items/buy': {
            'item_id': '',
            'count': '1',
            'currency': 'coins',
        },
        'items/equip': {
            'item_id': '',
        },
        'bank/get.prices': {},
        'achievements/get.list': {},
        'top/get.global': {},
        'top/get.month': {},
        'top/get.week': {},
        'top/get.day': {},
        'actions/get.prizes': {},
        'actions/reward.user': {
            'prize_type': '',
        },
        'trophies/get.collections': {},
        'trophies/get.users.trophies': {
            'user_id': '',
        },
        'game_sessions/init': {
            'difficulty': 'medium',
        },
        'game_sessions/finish': {
            'result': '',
        },
        'suite/repair': {
            'hp_points': '1',
            'currency': 'coins',
        },
        'user/explore.capsule': {
            'user_id': ''
        },
        'achievements/check/ping.earth': {},
        'achievements/check/be.trained': {},
        'achievements/check/picture': {},
        'achievements/check/banking': {},
    }

    template_name = 'testing.html'
    request = None
    selectedMethod = None

    def dispatch(self, request, *args, **kwargs):
        if not settings.DEBUG:
            return HttpResponse(
                content='Testing is not avaible',
                content_type='text/plain; charset=UTF-8',
                status=403,
            )
        self.request = request
        self.selectedMethod = kwargs.get('method_code')
        return super(TestingView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TestingView, self).get_context_data(**kwargs)
        if (self.request.method == 'POST') and ('execute' in self.request.POST):
            context.update({'response': self.__executeMethod()})
        context.update({
            'methods_list': sorted(self.METHODS_LIST.iteritems()),
            'selected_method': self.selectedMethod,
            'post': self.request.POST,
        })
        return context

    def __executeMethod(self):
        viewerId = self.request.POST.get('testing_soc_net_id')
        postParams = self.__getMethodParams()
        serverCallParams = self.__getServerCallParams(viewerId)
        return self.__makeRequest(postParams, serverCallParams, viewerId)

    def __getMethodParams(self):
        args = {}
        regex = re.compile(r'args\[(?P<name>[\w-]+)\]', re.IGNORECASE)
        for key in self.request.POST:
            data = regex.findall(key)
            if data:
                args[data[0]] = self.request.POST[key]
        return args

    def __getServerCallParams(self, viewerId):
        authKey = VkComSocNet.makeAuthKey(viewerId)
        return {
            'sid': u'1',
            'viewer_id': viewerId,
            'auth_key': authKey,
        }

    def __makeRequest(self, postParams, serverCallParams, viewerId):
        url = reverse('api/' + self.selectedMethod, kwargs={'soc_net': 'vk.com'})
        postParams.update(serverCallParams)
        postParams['token'] = self.__getToken(viewerId, url)
        requestBody = urlencode(postParams)
        requestHeaders = {
            'Content-type': 'application/x-www-form-urlencoded'
        }
        connection = HTTPConnection('127.0.0.1', 8000)
        try:
            connection.connect()
            connection.request('POST', url, requestBody, requestHeaders)
            response = connection.getresponse()
            httpStatus = response.status
            contentType = response.getheader('Content-Type')
            responseText = response.read()
            response.close()
            connection.close()
        finally:
            connection.close()

        try:
            responseDecoded = json.loads(responseText, encoding='UTF-8')
        except ValueError:
            responseDecoded = {}

        return {
            'status': httpStatus,
            'content_type': contentType,
            'text': responseText,
            'data': responseDecoded,
            'url': url,
            'post': postParams,
        }

    def __getToken(self, viewerId, requestUri):
        authKey = VkComSocNet.makeAuthKey(viewerId)
        vkCom = VkComSocNet(
            self.request,
            viewerId=viewerId,
            authKey=authKey,
            sid=u'1',
        )
        return vkCom.makeToken(requestUri)
