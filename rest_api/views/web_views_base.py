# coding=utf-8
from django.contrib.auth.views import redirect_to_login
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView

__author__ = 'Andre'


class WebView(TemplateView):
    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class ViewForAdmin(TemplateView):
    def dispatch(self, request, *args, **kwargs):
        """
        Только для админа
        """
        if not request.user.is_superuser:
            path = request.build_absolute_uri()
            return redirect_to_login(path, reverse('admin:index'))
        return super(ViewForAdmin, self).dispatch(request, *args, **kwargs)

