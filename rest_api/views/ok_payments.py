# coding=utf-8
import hashlib
import re

from django.conf import settings
from django.http import HttpResponse
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction

from rest_api.logic.user import UserLogic
from rest_api.logic.exceptions import NotCorrectError, SocNetParamsError, SocNetAuthError
from rest_api.models import Users, BankStatistics


__author__ = 'Andre'


ERROR_UNKNOWN = 1
ERROR_SERVICE = 2
ERROR_CALLBACK_INVALID_PAYMENT = 1001
ERROR_SYSTEM = 9999
ERROR_PARAM_SIGNATURE = 104


RESPONSE_OK = """<?xml version="1.0" encoding="UTF-8"?>
<callbacks_payment_response xmlns="http://api.forticom.com/1.0/">
true
</callbacks_payment_response>
"""

RESPONSE_ERROR_TPL = """<?xml version="1.0" encoding="UTF-8"?>
<ns2:error_response xmlns:ns2='http://api.forticom.com/1.0/'>
    <error_code>{error_code}</error_code>
    <error_msg>{error_message}</error_msg>
</ns2:error_response>
"""


def getCoinsPrice(count):
    count = int(count)

    pricesData = settings.GAME_SETTINGS['soc_net']['odnoklassniki_ru']['coins_prices']
    price = None
    for currentData in pricesData:
        if currentData['coins'] == count:
            price = currentData['price']
            break

    return price


def getGemsPrice(count):
    count = int(count)

    pricesData = settings.GAME_SETTINGS['soc_net']['odnoklassniki_ru']['gems_prices']
    price = None
    for currentData in pricesData:
        if currentData['gems'] == count:
            price = currentData['price']
            break

    return price


class OkPaymentsView(View):
    @transaction.commit_manually
    def get(self, request):
        self._request = request
        self._errorCode = None
        success = False
        try:
            self._setInput()
            self._makeAction()
            success = True
        except (NotCorrectError, SocNetParamsError), e:
            self._setErrorAnswer(e.message)
        except Exception, e:
            self._errorCode = ERROR_SYSTEM
            self._setErrorAnswer(e.message)
        finally:
            if success:
                transaction.commit()
            else:
                transaction.rollback()

        httpResponse = HttpResponse(self._actionResult, mimetype='application/xml')
        if self._errorCode:
            httpResponse['invocation-error'] = self._errorCode
        return httpResponse

    def _setInput(self):
        self._input = self._request.GET
        if self._input.get('method') != 'callbacks.payment':
            raise NotCorrectError('Wrong method')

        rightSigBase = ''
        keysList = sorted(self._input.iterkeys())
        for key in keysList:
            if key != 'sig':
                rightSigBase += key + '=' + self._input[key]
        rightSigBase += settings.GAME_SETTINGS['soc_net']['odnoklassniki_ru']['api_secret']
        rightSig = hashlib.md5(rightSigBase).hexdigest()

        self._userId = self._input.get('uid')
        if not self._userId:
            raise SocNetAuthError('User id is not defined')

        if self._input.get('sig') != rightSig:
            self._errorCode = ERROR_PARAM_SIGNATURE
            raise SocNetAuthError('Signature is invalid')

    def _makeAction(self):
        itemId = self._input.get('product_code', '')
        currency, count = self._getItemInfo(itemId)
        price = self._getPrice(currency, count)

        self._getUser()
        self._addMoney(currency, count)
        self._logMoney(currency, count)

        self._actionResult = RESPONSE_OK

    def _getItemInfo(self, itemId):
        matches = re.findall(r'^((?:coins)|(?:gems))_(\d+)$', itemId)
        if not matches or len(matches[0]) < 2:
            self._errorCode = ERROR_CALLBACK_INVALID_PAYMENT
            raise NotCorrectError('Item is invalid')
        return unicode(matches[0][0]), int(matches[0][1])

    def _getPrice(self, currency, count):
        if currency == 'coins':
            price = getCoinsPrice(count)
        else:
            price = getGemsPrice(count)
        if price is None:
            self._errorCode = ERROR_CALLBACK_INVALID_PAYMENT
            raise NotCorrectError('Wrong count')
        return price

    def _setErrorAnswer(self, message):
        if not self._errorCode:
            self._errorCode = ERROR_UNKNOWN
        params = {
            'error_code': self._errorCode,
            'error_message': message
        }
        self._actionResult = RESPONSE_ERROR_TPL.format(params)

    def _getUser(self):
        userId = self._userId
        if not userId:
            raise NotCorrectError('User id are not passed')
        try:
            self._user = Users.objects.get(soc_net_id=userId)
        except ObjectDoesNotExist:
            self._errorCode = ERROR_CALLBACK_INVALID_PAYMENT
            raise NotCorrectError('User does not exists')

    def _addMoney(self, currency, count):
        if currency == 'coins':
            self._user.coins += count
        else:
            self._user.gems += count
        self._user.save()
        UserLogic().clearUserInfoCache(self._user.soc_net_id)

    def _logMoney(self, currency, count):
        try:
            statRecord = BankStatistics.objects.get(coins=count)
        except ObjectDoesNotExist:
            statRecord = BankStatistics(coins=count)
        statRecord.count_global += 1
        statRecord.count_day += 1
        statRecord.count_week += 1
        statRecord.count_month += 1
        statRecord.save()
