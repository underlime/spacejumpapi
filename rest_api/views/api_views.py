# coding=utf-8
import datetime
import os
import time

from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import FileSystemStorage
from django.db import IntegrityError
from django.forms import model_to_dict

from spacejump import settings
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.top import TopLogic
from rest_api.logic.user import UserLogic
from rest_api.models import Users
from rest_api.response import top, actions
from rest_api.response.game_sessions import initGameSessionResp, finishGameSessionResp
from rest_api.response.lists import getItemsList, getAchievementsList
from rest_api.response.news import setNewsRead
from rest_api.response.repair import repairSuiteResp
from rest_api.response.trophies import getTrophiesCollectionsList, getUsersTrophies
from rest_api.response.user import getFullUserInfo, registerUser, buyItem, equipItem, getUsersListInfo, exploreCapsule, setPhotoAlbum
from rest_api.views.api_views_base import ApiView, AchievementsView


__author__ = 'Andre'


class ItemsGetMarketList(ApiView):
    def post(self, request, **kwargs):
        return self._renderJsonResponse(getItemsList())


class ItemsBuy(ApiView):
    def post(self, request, **kwargs):
        userId = self._userInfo['id']
        itemId = request.POST.get('item_id')
        currency = request.POST.get('currency', 'coins')
        count = request.POST.get('count', 1)
        return self._renderJsonResponse(buyItem(userId, itemId, count, currency, self._socNet))


class ItemsEquip(ApiView):
    def post(self, request, **kwargs):
        userId = self._userInfo['id']
        itemId = request.POST.get('item_id')
        return self._renderJsonResponse(equipItem(userId, itemId))


class BankGetPrices(ApiView):
    def post(self, request, **kwargs):
        socNet = self._socNet.getSocNetName()
        coinsPrices = settings.GAME_SETTINGS['soc_net'][socNet]['coins_prices']
        gemsPrices = settings.GAME_SETTINGS['soc_net'][socNet]['gems_prices']
        data = {
            'coins_prices': coinsPrices,
            'gems_prices': gemsPrices,
        }
        return self._renderJsonResponse(data)


class AchievementsGetList(ApiView):
    def post(self, request, **kwargs):
        return self._renderJsonResponse(getAchievementsList())


class UserGetInfo(ApiView):
    def __init__(self, **kwargs):
        super(UserGetInfo, self).__init__(**kwargs)
        self._needGetUserInfo = False

    def post(self, request, **kwargs):
        socNetId = request.POST.get('soc_net_id')
        baseInfoOnly = request.POST.get('base_info_only', '0')

        if not socNetId:
            socNetId = self._socNet.getSocNetId()
            handleVisitsInfo = True
        else:
            handleVisitsInfo = False

        try:
            return self._renderJsonResponse(getFullUserInfo(socNetId, baseInfoOnly, handleVisitsInfo))
        except ObjectDoesNotExist:
            return self._renderError(code=401, message='Registration need')


class UserGetListInfo(ApiView):
    def __init__(self, **kwargs):
        super(UserGetListInfo, self).__init__(**kwargs)
        self._needGetUserInfo = False

    def post(self, request, **kwargs):
        socNetIdsString = request.POST.get('soc_net_ids_list', '')
        return self._renderJsonResponse(getUsersListInfo(socNetIdsString))


class UserRegister(ApiView):
    def __init__(self, **kwargs):
        super(UserRegister, self).__init__(**kwargs)
        self._needGetUserInfo = False

    def post(self, request, **kwargs):
        socNetId = self._socNet.getSocNetId()
        referrer = request.POST.get('referrer')
        try:
            return self._renderJsonResponse(registerUser(socNetId=socNetId, referrer=referrer))
        except IntegrityError:
            return self._renderError(code=409, message='User already exists')


class UserGetCoins(ApiView):
    def post(self, request, **kwargs):
        socNetId = self._socNet.getSocNetId()
        userInfo = Users.objects.get(soc_net_id=socNetId)
        data = {
            'user': {
                'coins': userInfo.coins,
                'gems': userInfo.gems,
            }
        }
        return self._renderJsonResponse(data)


class UserExploreCapsule(ApiView):
    def post(self, request, **kwargs):
        explorerUserId = self._userInfo['id']
        userId = request.POST.get('user_id')
        return self._renderJsonResponse(exploreCapsule(explorerUserId, userId))


class UserSetPhotoAlbum(ApiView):
    def post(self, request, **kwargs):
        userId = self._userInfo['id']
        photoAlbumId = request.POST.get('photo_album_id')
        return self._renderJsonResponse(setPhotoAlbum(userId, photoAlbumId))


class UserSetNewsRead(ApiView):
    def post(self, request, *args, **kwargs):
        newsId = request.POST.get('news_id')
        isShared = request.POST.get('is_shared')
        data = setNewsRead(self._userInfo['id'], newsId, isShared)
        return self._renderJsonResponse(data)


class UserGiveFriendsBonuses(ApiView):
    def post(self, request, *args, **kwargs):
        bonusType = request.POST.get('bonus_type')
        locale = request.POST.get('locale', 'en')
        userModel = UserLogic().giveFriendsBonuses(self._socNet, bonusType, locale)
        data = {'user': model_to_dict(userModel)}
        return self._renderJsonResponse(data)


class TopGlobal(ApiView):
    def post(self, request, **kwargs):
        data = top.getTop(TopLogic.TOP_GLOBAL)
        return self._renderJsonResponse(data)


class TopMonth(ApiView):
    def post(self, request, **kwargs):
        data = top.getTop(TopLogic.TOP_MONTH)
        return self._renderJsonResponse(data)


class TopWeek(ApiView):
    def post(self, request, **kwargs):
        data = top.getTop(TopLogic.TOP_WEEK)
        return self._renderJsonResponse(data)


class TopDay(ApiView):
    def post(self, request, **kwargs):
        data = top.getTop(TopLogic.TOP_DAY)
        return self._renderJsonResponse(data)


class ActionsGetPrizes(ApiView):
    def post(self, request, **kwargs):
        data = actions.getPrizes()
        return self._renderJsonResponse(data)


class ActionsRewardUser(ApiView):
    def post(self, request, **kwargs):
        socNetId = self._socNet.getSocNetId()
        prizeType = request.POST.get('prize_type')
        data = actions.rewardUser(socNetId, prizeType)
        return self._renderJsonResponse(data)


class TrophiesGetCollections(ApiView):
    def post(self, request, **kwargs):
        return self._renderJsonResponse(getTrophiesCollectionsList())


class TrophiesGetUsersTrophies(ApiView):
    def post(self, request, **kwargs):
        requestUserId = request.POST.get('user_id')
        if requestUserId and str(requestUserId).isdigit():
            userId = long(requestUserId)
        else:
            userId = self._userInfo['id']
        return self._renderJsonResponse(getUsersTrophies(userId))


class GameSessionsInit(ApiView):
    def post(self, request, **kwargs):
        userId = self._userInfo['id']
        difficulty = request.POST.get('difficulty')
        return self._renderJsonResponse(initGameSessionResp(userId, difficulty))


class GameSessionsFinish(ApiView):
    def post(self, request, **kwargs):
        result = request.POST.get('result')
        userId = self._userInfo['id']
        return self._renderJsonResponse(finishGameSessionResp(userId, result, self._socNet))


class SuiteRepair(ApiView):
    def post(self, request, **kwargs):
        userId = self._userInfo['id']
        hpPoints = request.POST.get('hp_points', 0)
        currency = request.POST.get('currency', 'coins')
        return self._renderJsonResponse(repairSuiteResp(userId, hpPoints, currency, self._socNet))


class AchievementsCheckPingEarth(AchievementsView):
    def __init__(self, **kwargs):
        super(AchievementsCheckPingEarth, self).__init__(**kwargs)
        self._code = 'ping_home'


class AchievementsCheckBeTrained(AchievementsView):
    def __init__(self, **kwargs):
        super(AchievementsCheckBeTrained, self).__init__(**kwargs)
        self._code = 'Train_hard'


class AchievementsCheckPicture(AchievementsView):
    def __init__(self, **kwargs):
        super(AchievementsCheckPicture, self).__init__(**kwargs)
        self._code = 'Picture'


class AchievementsCheckBanking(AchievementsView):
    def __init__(self, **kwargs):
        super(AchievementsCheckBanking, self).__init__(**kwargs)
        self._code = 'Banking'


class ImageUpload(ApiView):
    def post(self, request, *args, **kwargs):
        if self._socNet.getSocNetName() != 'my_mail_ru':
            raise NotCorrectError('Wrong request type')

        fileRecord = request.FILES.get('file')
        if not fileRecord:
            raise NotCorrectError('No file')

        fileBaseName, fileExt = os.path.splitext(fileRecord.name)
        if fileExt not in ('.jpg', '.jpeg', '.png', '.gif'):
            raise NotCorrectError('File user type is not image')

        nowTime = datetime.datetime.utcnow()
        dirName = '{0}/{1}/{2}/'.format(
            nowTime.year,
            nowTime.month,
            nowTime.day
        )
        timestamp = time.mktime(nowTime.timetuple())
        fileName = str(timestamp) + '-' + fileRecord.name

        storage = FileSystemStorage(location=settings.MEDIA_ROOT + dirName)
        storage.save(fileName, fileRecord)

        httpHost = request.META.get('HTTP_HOST')
        if not httpHost:
            raise NotCorrectError('No http host')

        fileUrl = 'http://' + httpHost + settings.MEDIA_URL + dirName + fileName
        return self._renderJsonResponse({'url': fileUrl})
