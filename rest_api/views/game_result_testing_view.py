import base64
import hashlib
import json
from django.conf import settings
from django.http import HttpResponse
from rest_api.views.web_views_base import ViewForAdmin, WebView

__author__ = 'andre'


class GameResultTestingView(ViewForAdmin, WebView):
    template_name = 'game_result_testing.html'

    def dispatch(self, request, *args, **kwargs):
        if not settings.DEBUG:
            return HttpResponse(
                content='Testing is not avaible',
                content_type='text/plain; charset=UTF-8',
                status=403,
            )
        self.request = request
        self.selectedMethod = kwargs.get('method_code')
        return super(GameResultTestingView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GameResultTestingView, self).get_context_data(**kwargs)
        if self.request.method == 'POST' and 'session_id' in self.request.POST:
            context = self._buildObject(context)
        return context

    def _buildObject(self, context):
        data = {}
        fieldsList = ('session_id', 'win', 'score', 'bronze_coins', 'silver_coins', 'gold_coins', 'trophy_found',
                      'cola', 'distance', 'booster_count', 'power_shield_count', 'heart_count', 'shield_count',
                      'items_used')
        sigBase = ''
        for field in fieldsList:
            data[field] = self.request.POST.get(field)
            sigBase += data[field] + ':'
        sigBase += self.request.POST.get('salt')

        sigMd5 = hashlib.md5()
        sigMd5.update(sigBase)
        data['sig'] = sigMd5.hexdigest()

        strItemsUsed = str(data['items_used'])
        if strItemsUsed:
            data['items_used'] = strItemsUsed.split(',')
        else:
            data['items_used'] = []

        jsonData = json.dumps(data)
        base64Data = base64.b64encode(jsonData)
        context.update({
            'result': {
                'json_data': jsonData,
                'base64_data': base64Data
            }
        })
        return context
