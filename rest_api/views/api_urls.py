from django.conf.urls import patterns, url
from rest_api.views.api_views import *

__author__ = 'Andre'


def getUrls():
    return patterns(
        '',
        url(r'^items/get.market.list/$', ItemsGetMarketList.as_view(), name=r'api/items/get.market.list'),
        url(r'^items/buy/$', ItemsBuy.as_view(), name=r'api/items/buy'),
        url(r'^items/equip/$', ItemsEquip.as_view(), name=r'api/items/equip'),
        url(r'^bank/get.prices/$', BankGetPrices.as_view(), name=r'api/bank/get.prices'),
        url(r'^achievements/get.list/$', AchievementsGetList.as_view(), name=r'api/achievements/get.list'),
        url(r'^user/get.info/$', UserGetInfo.as_view(), name=r'api/user/get.info'),
        url(r'^user/get.list.info/$', UserGetListInfo.as_view(), name=r'api/user/get.list.info'),
        url(r'^user/get.coins/$', UserGetCoins.as_view(), name=r'api/user/get.coins'),
        url(r'^user/register/$', UserRegister.as_view(), name=r'api/user/register'),
        url(r'^user/explore.capsule/$', UserExploreCapsule.as_view(), name=r'api/user/explore.capsule'),
        url(r'^user/set.photo.album/$', UserSetPhotoAlbum.as_view(), name=r'api/user/set.photo.album'),
        url(r'^user/give.friends.bonuses/$', UserGiveFriendsBonuses.as_view(), name=r'api/user/give.friends.bonuses'),
        url(r'^user/user.set.news.read/$', UserSetNewsRead.as_view(), name=r'api/user/user.set.news.read'),
        url(r'^top/get.global/$', TopGlobal.as_view(), name=r'api/top/get.global'),
        url(r'^top/get.month/$', TopMonth.as_view(), name=r'api/top/get.month'),
        url(r'^top/get.week/$', TopWeek.as_view(), name=r'api/top/get.week'),
        url(r'^top/get.day/$', TopDay.as_view(), name=r'api/top/get.day'),
        url(r'^actions/get.prizes/$', ActionsGetPrizes.as_view(), name='api/actions/get.prizes'),
        url(r'^actions/reward.user/$', ActionsRewardUser.as_view(), name='api/actions/reward.user'),
        url(r'^trophies/get.collections/$', TrophiesGetCollections.as_view(), name='api/trophies/get.collections'),
        url(r'^trophies/get.users.trophies/$', TrophiesGetUsersTrophies.as_view(),
            name='api/trophies/get.users.trophies'),
        url(r'^game_sessions/init/$', GameSessionsInit.as_view(), name='api/game_sessions/init'),
        url(r'^game_sessions/finish/$', GameSessionsFinish.as_view(), name='api/game_sessions/finish'),
        url(r'^suite/repair/$', SuiteRepair.as_view(), name='api/suite/repair'),

        url(r'^achievements/check/ping.earth/$', AchievementsCheckPingEarth.as_view(),
            name='api/achievements/check/ping.earth'),
        url(r'^achievements/check/be.trained/$', AchievementsCheckBeTrained.as_view(),
            name='api/achievements/check/be.trained'),
        url(r'^achievements/check/picture/$', AchievementsCheckPicture.as_view(),
            name='api/achievements/check/picture'),
        url(r'^achievements/check/banking/$', AchievementsCheckBanking.as_view(),
            name='api/achievements/check/banking'),
        url(r'^image/upload/$', ImageUpload.as_view(), name='api/image/upload'),
    )
