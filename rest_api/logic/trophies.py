from rest_api import cache
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.logic import Logic
from rest_api.models import Trophies, TrophiesCollections, UsersTrophies
from rest_api.utils import buildModelResponseStruct

__author__ = 'Andre'


class TrophiesLogic(Logic):
    @cache.cachedFunction('trophies_collections')
    def getTrophiesCollectionsList(self):
        modelData = TrophiesCollections.objects.all()
        return buildModelResponseStruct(modelData)

    @cache.cachedFunction('trophies_list')
    def getTrophiesList(self):
        data = Trophies.objects.all()
        return buildModelResponseStruct(data)

    @cache.cachedFunction('trophies_list_group_by_collection')
    def getTrophiesGroupByCollection(self):
        data = Trophies.objects.all()
        trophiesList = buildModelResponseStruct(data)

        trophiesGroups = {}
        for listId in trophiesList:
            trophyData = trophiesList[listId]
            if trophiesGroups.get(trophyData['trophies_collection']) is None:
                trophiesGroups[trophyData['trophies_collection']] = {}
            trophiesGroups[trophyData['trophies_collection']][listId] = trophyData

        return trophiesGroups

    @cache.cachedFunction('users_trophies')
    def getUsersTrophies(self, userId):
        if not str(userId).isdigit():
            raise NotCorrectError('Wrong user id format')
        modelData = UsersTrophies.objects.filter(user_id=userId)
        return buildModelResponseStruct(modelData)
