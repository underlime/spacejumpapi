import random
import datetime
import time
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from rest_api import cache
from rest_api.logic.achievements import checkAchievementByCode, AchievementReceivedError
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.game_sessions_constants import SIG_FIELDS_LIST, TROPHIES_IN_COLLECTION, DISTANCES_TABLE, MAX_TIME_ERROR, MAX_VELOCITY, DIFFICULTIES_TABLE
from rest_api.logic.lists import ListsLogic
from rest_api.logic.repair import getSuiteParams, setSuiteDamage, autoRepairSuite
from rest_api.models import GameSessions, Trophies, UsersTrophies, Users, Inventory, Items, GameSessionsLog
from spacejump.settings import GAME_SETTINGS


__author__ = 'Andre'


def initGameSession(userId, difficulty):
    if difficulty not in GameSessions.DIFFICULTIES_LIST:
        raise NotCorrectError('Difficulty invalid')

    oldSession = GameSessions.objects.filter(user_id=userId).order_by('-start_time')
    if len(oldSession) and \
            (time.mktime(datetime.datetime.utcnow().timetuple())
                 - time.mktime(oldSession[0].start_time.timetuple()) < 5):
        raise NotCorrectError('Too fast')

    suiteParams, itemInfo = getSuiteParams(userId)
    if GAME_SETTINGS['suite_damage']['enabled_damage'] and suiteParams.hp < 1:
        raise NotCorrectError('Not enougn suite HP')

    gameSession = GameSessions()
    gameSession.user_id = userId
    gameSession.trophy_flag = (random.random() < GAME_SETTINGS['trophy_probability'])
    gameSession.start_time = datetime.datetime.utcnow()
    gameSession.difficulty = difficulty
    gameSession.save()

    if settings.DEBUG:
        print 'Init game session:', gameSession.id

    return gameSession, suiteParams


def finishGameSession(userId, sessionData, itemsUsed):
    for fieldName in SIG_FIELDS_LIST:
        if sessionData[fieldName] < 0:
            message = 'Negative {0} value'.format(fieldName)
            raise NotCorrectError(message)

    if settings.DEBUG:
        print 'Finish game session:', sessionData['session_id']

    try:
        gameSession = GameSessions.objects.get(id=sessionData['session_id'])
    except ObjectDoesNotExist:
        raise NotCorrectError('Session {0} does not exists'.format(sessionData['session_id']))

    sessionSeconds = time.mktime(datetime.datetime.utcnow().timetuple()) - time.mktime(
        gameSession.start_time.timetuple())
    if sessionSeconds < 1:
        raise NotCorrectError('Too fast')

    if gameSession.user_id != userId:
        raise NotCorrectError('Wrong user id')

    userInfo = Users.objects.get(id=userId)
    userInfo.jumps_count += 1
    _checkSessionData(userInfo, sessionSeconds, gameSession, sessionData)

    if gameSession.trophy_flag and sessionData['trophy_found']:
        trophy, usersTrophy = _addTrophyToUser(userId)
    elif sessionData['trophy_found']:
        raise NotCorrectError('Wrong trophy flag')
    else:
        trophy = None
        usersTrophy = None

    if sessionData['win']:
        suiteRecord = autoRepairSuite(userId, gameSession.difficulty)
    elif GAME_SETTINGS['suite_damage']['enabled_damage']\
            and userInfo.jumps_count > GAME_SETTINGS['suite_damage']['free_jumps']:
        suiteRecord = setSuiteDamage(userId)
    else:
        suiteRecord = None

    if itemsUsed:
        inventory = Inventory.objects.filter(user_id=userId, item_id__in=itemsUsed)
        resurrected = _robItems(inventory, itemsUsed)
    else:
        inventory = None
        resurrected = 0

    userInfo.coins += sessionData['coins']
    if sessionData['gems_spent']:
        newGems = userInfo.gems - sessionData['gems_spent']
        if newGems < 0:
            raise NotCorrectError('Negative gems')
        userInfo.gems = newGems
        resurrected += sessionData['gems_resurrections_count']

    if sessionData['win']:
        userInfo.landings_count += 1

    userInfo.summary_time += sessionSeconds
    newScore = _setNewScore(userInfo, sessionData['score'])

    userAchievementsList = []
    if userInfo.landings_count < 3:
        _checkFirstJumpAchievements(userInfo, sessionData['win'], userAchievementsList)

    if sessionData['win']:
        _checkFinishAchievements(userInfo, gameSession, userAchievementsList)

    if resurrected:
        _checkResurrectionAchievements(userInfo, resurrected, userAchievementsList)

    if trophy:
        _checkTrophiesAchievements(userInfo, trophy, userAchievementsList)

    _checkJumpCountAchievements(userInfo, userAchievementsList)
    _checkCoinsAchievements(userInfo, sessionData['coins'], userAchievementsList)

    userInfo.save()

    _logGameSession(userInfo, sessionSeconds, gameSession, sessionData)

    cache.delDataFromCache('user_info', (userInfo.soc_net_id,))
    cache.delDataFromCache('inventory', (userId,))
    if usersTrophy is not None:
        cache.delDataFromCache('users_trophies', (userId,))
    if userAchievementsList:
        cache.delDataFromCache('user_achievements', (userId,))

    gameSession.delete()
    return {
        'trophy': usersTrophy,
        'suite_record': suiteRecord,
        'user_info': userInfo,
        'new_score': newScore,
        'inventory': inventory,
        'user_achievements_list': userAchievementsList,
    }


def _addTrophyToUser(userId):
    usersAlreadyTrophies = UsersTrophies.objects.filter(user_id=userId)
    notTropList = []
    for trophyRecord in usersAlreadyTrophies:
        notTropList.append(trophyRecord.trophy_id)
    trophiesList = Trophies.objects.filter(~Q(id__in=notTropList)).order_by('?')
    if len(trophiesList):
        usersTrophy = UsersTrophies()
        usersTrophy.user_id = userId
        trophy = trophiesList[0]
        usersTrophy.trophy_id = trophy.id
        usersTrophy.save()
    else:
        trophy = None
        usersTrophy = None

    return trophy, usersTrophy


def _setNewScore(userInfo, score):
    newScore = False
    if score > userInfo.max_score_day:
        userInfo.max_score_day = score
        newScore = True
    if score > userInfo.max_score_week:
        userInfo.max_score_week = score
        newScore = True
    if score > userInfo.max_score_month:
        userInfo.max_score_month = score
        newScore = True
    if score > userInfo.max_score_global:
        userInfo.max_score_global = score
        newScore = True

    return newScore


def _checkFirstJumpAchievements(userInfo, isWin, userAchievementsList):
    try:
        if isWin:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'sweat', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        else:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'first_blood', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass


def _checkFinishAchievements(userInfo, gameSession, userAchievementsList):
    try:
        if gameSession.difficulty == GameSessions.DIFFICULTY.MEDIUM:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'normal_mode', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        elif gameSession.difficulty == GameSessions.DIFFICULTY.HARD:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Iron_Felix', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass


def _checkJumpCountAchievements(userInfo, userAchievementsList):
    try:
        if userInfo.jumps_count == 30:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'noob', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        elif userInfo.jumps_count == 200:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Battle-seasoned', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        elif userInfo.jumps_count == 500:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'expert', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        elif userInfo.jumps_count == 3000:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Cosmo-Diver', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass


def _checkResurrectionAchievements(userInfo, resurrected, userAchievementsList):
    try:
        if resurrected >= 1:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Last-chance', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass

    try:
        if resurrected >= 2:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'vitality', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass

    try:
        if resurrected >= 5:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Will-live', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass

    try:
        if resurrected >= 20:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'immortal', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass


def _checkTrophiesAchievements(userInfo, trophy, userAchievementsList):
    try:
        userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'what', userInfo, False)
        if userAchv:
            userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass

    collectionCount = UsersTrophies.objects.filter(user_id=userInfo.id,
                                                   trophy__trophies_collection_id=trophy.trophies_collection_id).count()
    if collectionCount >= TROPHIES_IN_COLLECTION:
        try:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'non-profession', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        except AchievementReceivedError:
            pass

    allCount = UsersTrophies.objects.filter(user_id=userInfo.id).count()
    try:
        if allCount == 10:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'collector', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        elif allCount == 15:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'nutty', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        elif allCount == 20:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'mega-collector', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
        elif allCount == 30:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'caretaker', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass


def _checkCoinsAchievements(userInfo, coinsCount, userAchievementsList):
    try:
        if coinsCount >= 100:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'trifle', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass

    try:
        if coinsCount >= 500:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Pocket-money', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass

    try:
        if coinsCount >= 1000:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'salary', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass

    try:
        if coinsCount >= 2000:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Award', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass

    try:
        if coinsCount >= 5000:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'bag-of-money', userInfo, False)
            if userAchv:
                userAchievementsList.append(userAchv)
    except AchievementReceivedError:
        pass


def _robItems(inventory, itemsUsed):
    itemsList = ListsLogic().getItemsList()
    resurrectionsIdsList = []
    for itemId in itemsList:
        if itemsList[itemId]['action'] in (Items.ACTION_RESURRECTION, Items.ACTION_RESURRECTION_PLUS):
            resurrectionsIdsList.append(long(itemsList[itemId]['id']))

    usedItemsTable = {}
    for itemId in itemsUsed:
        itemId = long(itemId)
        if itemId not in usedItemsTable:
            usedItemsTable[itemId] = 0
        usedItemsTable[itemId] += 1

    resurrected = 0
    for invRecord in inventory:
        usedCount = long(usedItemsTable.get(invRecord.item_id, 0))
        if invRecord.item_id in resurrectionsIdsList:
            resurrected += usedCount
        currentCount = invRecord.count
        currentCount -= usedCount
        if currentCount < 0:
            raise NotCorrectError('Too many items used')
        invRecord.count = currentCount
        invRecord.save()

    return resurrected


def _checkSessionData(userInfo, gameTime, gameSession, sessionData):
    _checkSpaceTimeBalance(sessionData, gameTime)
    _checkMoneyBalance(sessionData)
    _checkScoreBalance(userInfo, gameSession, sessionData)


def _checkSpaceTimeBalance(sessionData, gameTime):
    timeBalance = gameTime - sessionData['real_time']
    if abs(timeBalance) > MAX_TIME_ERROR:
        message = 'Time error is too large: {0}\nST: {1} RT: {2}'
        raise NotCorrectError(message.format(timeBalance, gameTime, sessionData['real_time']))

    if sessionData['max_velocity'] > MAX_VELOCITY:
        raise NotCorrectError('Max velocity is too large')

    maxDistance = sessionData['physics_tics'] * sessionData['max_velocity']
    if 0 < maxDistance < sessionData['distance']:
        raise NotCorrectError('Wrong distance')


def _checkMoneyBalance(sessionData):
    maxCoins = sessionData['bronze_coins'] + 2 * sessionData['silver_coins'] + 3 * sessionData['gold_coins']
    if sessionData['coins'] > maxCoins:
        raise NotCorrectError('Wrong coins count')
    if sessionData['gems_spent']:
        rightGemsSpent = pow(2, sessionData['gems_resurrections_count']) - 1
        if sessionData['gems_spent'] != rightGemsSpent:
            raise NotCorrectError('Wrong gems count')


def _checkScoreBalance(userInfo, gameSession, sessionData):
    maxError = sessionData['distance'] * 0.002

    maxDistance = DISTANCES_TABLE.get(gameSession.difficulty)
    if maxDistance is not None:
        balance = maxDistance - sessionData['distance']
        if balance < 0 and abs(balance) > maxError:
            raise NotCorrectError('Incorrect distance')

    sumFieldsList = ('bronze_coins', 'silver_coins', 'gold_coins', 'cola',
                     'distance', 'booster_count', 'power_shield_count', 'heart_count',
                     'shield_count',)
    valuesSum = 0
    for fieldName in sumFieldsList:
        valuesSum += sessionData[fieldName]

    if sessionData['trophy_found']:
        valuesSum += 1

    starsSum = valuesSum * (1 + userInfo.stars * 0.01)
    balance = abs(sessionData['score'] - starsSum)
    if balance > maxError:
        raise NotCorrectError('There is no score balance: {0}'.format(balance))


def _logGameSession(userInfo, sessionSeconds, gameSession, sessionData):
    logRecord = GameSessionsLog()

    logRecord.user_id = userInfo.id
    logRecord.time = datetime.datetime.utcnow()
    logRecord.session_time = sessionSeconds
    logRecord.difficulty = DIFFICULTIES_TABLE.get(gameSession.difficulty, 0)

    logRecord.win = bool(sessionData['win'])

    logRecord.bronze_coins = sessionData['bronze_coins']
    logRecord.silver_coins = sessionData['silver_coins']
    logRecord.gold_coins = sessionData['gold_coins']
    logRecord.coins = sessionData['coins']

    logRecord.score = sessionData['score']
    logRecord.trophy_found = bool(sessionData['trophy_found'])

    logRecord.cola = sessionData['cola']
    logRecord.distance = sessionData['distance']
    logRecord.booster_count = sessionData['booster_count']
    logRecord.power_shield_count = sessionData['power_shield_count']
    logRecord.heart_count = sessionData['heart_count']
    logRecord.shield_count = sessionData['shield_count']
    logRecord.pause_seconds = sessionData['pause_seconds']
    logRecord.bonus_seconds = sessionData['bonus_seconds']
    logRecord.seconds = sessionData['seconds']
    logRecord.max_velocity = sessionData['max_velocity']

    logRecord.save()
