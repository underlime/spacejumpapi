# coding=utf-8
import datetime
import random
import time

from django.core.exceptions import ObjectDoesNotExist
from django.forms.models import model_to_dict
from rest_api.logic import news
from rest_api.logic.achievements import checkAchievementByCode, AchievementReceivedError

from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.lists import ListsLogic
from rest_api.logic.logic import Logic
from rest_api.models import Users, Inventory, UsersAchievements, Items, ShopStatistics
from rest_api import cache
from rest_api.utils import buildModelResponseStruct
from spacejump.settings import GAME_SETTINGS


__author__ = 'Andre'


PRIZE_COINS = 'coins'
PRIZE_ITEM = 'item'
PRIZE_GEM = 'gem'

PRIZES_ACTIONS_TABLE = {
    2: Items.ACTION_ROCKET,
    3: Items.ACTION_RESURRECTION,
    4: Items.ACTION_PARACHUTE,
    5: Items.ACTION_MEGA_ROCKET,
    6: Items.ACTION_RESURRECTION_PLUS
}


class UserLogic(Logic):
    BONUS_COINS = 'coins'
    BONUS_GEM = 'gem'
    BONUS_RESURRECTIONS = Items.ACTION_RESURRECTION
    BONUS_PARACHUTES = Items.ACTION_PARACHUTE
    BONUS_ROCKET = Items.ACTION_ROCKET

    @cache.cachedFunction('user_info')
    def getUserInfo(self, socNetId):
        userInfo = model_to_dict(Users.objects.get(soc_net_id=socNetId))
        return userInfo

    def getUsersListInfo(self, socNetIdsList):
        if len(socNetIdsList) == 0:
            raise NotCorrectError('Ids list is empty')
        usersListInfo = {}
        for socNetId in socNetIdsList:
            try:
                usersListInfo[socNetId] = self.getUserInfo(socNetId)
            except ObjectDoesNotExist:
                pass
        return usersListInfo

    def handleLastVisitInfo(self, socNetId):
        userModel = Users.objects.get(soc_net_id=socNetId)

        lastVisitDay = userModel.last_visit_date.toordinal()
        nowDate = datetime.datetime.utcnow()
        nowDay = nowDate.toordinal()
        dltDay = nowDay - lastVisitDay

        change = False
        if dltDay == 1:
            userModel.visits_count += 1
            change = True
        elif dltDay > 1:
            userModel.visits_count = 1
            change = True

        if change:
            userModel.last_visit_date = nowDate
            prizeType = self._awardForVisits(userModel)
            userModel.save()
        else:
            prizeType = None

        return prizeType

    def _awardForVisits(self, userModel):
        count = userModel.visits_count % 7
        itemAction = None
        if count == 1:
            userModel.coins += GAME_SETTINGS['first_day_prize']
            prizeType = PRIZE_COINS
        elif 2 <= count <= 6:
            itemAction = PRIZES_ACTIONS_TABLE.get(count)
            prizeType = PRIZE_ITEM
        else:
            userModel.gems += GAME_SETTINGS['last_day_prize']
            prizeType = PRIZE_GEM

        if prizeType == PRIZE_ITEM and itemAction:
            itemFinder = Items.objects.filter(action=itemAction).order_by('?')
            if len(itemFinder) > 0:
                itemInfo = itemFinder[0]
                try:
                    invRecord = Inventory.objects.get(
                        user_id=userModel.id,
                        item_id=itemInfo
                    )
                except ObjectDoesNotExist:
                    invRecord = Inventory(
                        user_id=userModel.id,
                        item_id=itemInfo.id,
                        count=0,
                    )
                invRecord.count += 1
                invRecord.save()

        return prizeType

    @cache.cachedFunction('inventory')
    def getInventory(self, userId):
        if not str(userId).isdigit():
            raise NotCorrectError('Wrong user id format')
        inventoryModel = Inventory.objects.filter(user_id=userId)
        inventory = buildModelResponseStruct(inventoryModel)
        return inventory

    def buyItem(self, userId, itemId, count, currency, socNet):
        if not (str(userId).isdigit() and str(itemId).isdigit() and str(count).isdigit()):
            raise NotCorrectError('Incorrect argument format')
        count = long(count)
        if count < 1:
            raise NotCorrectError('Incorrect count')

        itemInfo = ListsLogic().getItemInfo(itemId)
        if itemInfo['max_to_buy'] == 0:
            raise NotCorrectError('Unable to buy this item')
        elif itemInfo['rubric'] == Items.RUBRIC_UPGRADES:
            count = 1

        if itemInfo['rubric'] == Items.RUBRIC_UPGRADES and currency == 'coins':
            raise NotCorrectError('You can\'t buy upgrade for coins')

        inventoryRecord = self._addItemToUser(userId, itemId, count)
        price = self._getItemPrice(itemInfo, inventoryRecord, count, currency)
        self.pay(userId, price, currency)

        if 0 < itemInfo['max_to_buy'] < inventoryRecord.count:
            raise NotCorrectError('Max to buy exceeded')

        inventoryRecord.save()

        userInfo, achievementsList = self._checkBuyAchievements(userId, itemInfo, inventoryRecord)
        if achievementsList:
            socNet.setUserLevel(userInfo.soc_net_id, userInfo.stars)

        self._logBuyItem(itemInfo['id'])
        cache.delDataFromCache('inventory', (userId,))

        return inventoryRecord, userInfo, achievementsList

    def _addItemToUser(self, userId, itemId, count):
        try:
            inventoryRecord = Inventory.objects.get(user_id=userId, item_id=itemId)
            inventoryRecord.isNewRecord = False
        except ObjectDoesNotExist:
            inventoryRecord = Inventory()
            inventoryRecord.user_id = userId
            inventoryRecord.item_id = itemId
            inventoryRecord.count = 0
            inventoryRecord.isNewRecord = True

        inventoryRecord.count += count
        return inventoryRecord

    def _getItemPrice(self, itemInfo, inventoryRecord, count, currency):
        if itemInfo['upgrade_price']:
            return self._getItemUpgradePrice(itemInfo, inventoryRecord, count, currency)
        else:
            basePrice = itemInfo['price'] if currency == 'coins' else itemInfo['gems_price']
            return basePrice * count

    def _getItemUpgradePrice(self, itemInfo, inventoryRecord, count, currency):
        if currency == 'coins':
            pricesList = [itemInfo['price']]
            upgradePricesString = itemInfo['upgrade_price']
        else:
            pricesList = [itemInfo['gems_price']]
            upgradePricesString = itemInfo['gems_upgrade_price']

        upgradePricesList = (long(upPrice.strip()) for upPrice in upgradePricesString.split(','))
        pricesList += list(upgradePricesList)

        prevCount = inventoryRecord.count - 1
        index = prevCount
        if index >= len(pricesList):
            return pricesList[0] * prevCount
        else:
            return pricesList[index]

    def _checkBuyAchievements(self, userId, itemInfo, inventoryRecord):
        achievementsList = []

        userInfo = Users.objects.get(id=userId)
        needSave = False

        if itemInfo['rubric'] == Items.RUBRIC_UPGRADES:
            needSave = self._checkUpgradesAchievements(userInfo, itemInfo, inventoryRecord, achievementsList)
        elif itemInfo['rubric'] == Items.RUBRIC_CAPSULE:
            needSave = self._checkCapsulesAchievements(userInfo, itemInfo, achievementsList)
        elif itemInfo['rubric'] == Items.RUBRIC_HATS:
            needSave = self._checkHatsAchievements(userInfo, achievementsList)
        elif itemInfo['rubric'] == Items.RUBRIC_SUITS:
            needSave = self._checkSuitesAchievements(userInfo, itemInfo, achievementsList)

        if needSave:
            userInfo.save()

        return userInfo, achievementsList

    def _checkUpgradesAchievements(self, userInfo, itemInfo, inventoryRecord, achievementsList):
        needSave = False

        try:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'first_step', userInfo, False)
            if userAchv:
                achievementsList.append(userAchv)
                needSave = True
        except AchievementReceivedError:
            pass

        if inventoryRecord.count == Items.UPGRADES_STADIES_COUNT:
            try:
                userAchv = None

                if itemInfo['action'] == Items.ACTION_HEALTH:
                    userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'sturdy-child', userInfo, False)
                elif itemInfo['action'] == Items.ACTION_ACCELERATION:
                    userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'supersonic', userInfo, False)
                elif itemInfo['action'] == Items.ACTION_IMMORTALITY:
                    userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'monolith', userInfo, False)
                elif itemInfo['action'] == Items.ACTION_IMPROVE_COINS:
                    userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Klondike', userInfo, False)

                if userAchv:
                    achievementsList.append(userAchv)
                    needSave = True
            except AchievementReceivedError:
                pass

        upgradesCount = self._getUpgradesCount()
        userUpgrades = Inventory.objects.filter(user_id=userInfo.id, item__rubric=Items.RUBRIC_UPGRADES)

        if len(userUpgrades) == upgradesCount:
            try:
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'argument', userInfo, False)
                if userAchv:
                    achievementsList.append(userAchv)
                    needSave = True
            except AchievementReceivedError:
                pass

            allUpgrades = True
            for upgradeRecord in userUpgrades:
                if upgradeRecord.count < Items.UPGRADES_STADIES_COUNT:
                    allUpgrades = False
                    break

            if allUpgrades:
                try:
                    userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'perfection', userInfo, False)
                    if userAchv:
                        achievementsList.append(userAchv)
                        needSave = True
                except AchievementReceivedError:
                    pass

        return needSave

    def _checkCapsulesAchievements(self, userInfo, itemInfo, achievementsList):
        needSave = False

        try:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Like-new', userInfo, False)
            if userAchv:
                achievementsList.append(userAchv)
                needSave = True
        except AchievementReceivedError:
            pass

        try:
            userAchv = None

            if itemInfo['code'] == 'hi_tech_capsule':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'progress', userInfo, False)
            elif itemInfo['code'] == 'gold_capsule':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Arthur', userInfo, False)

            if userAchv:
                achievementsList.append(userAchv)
                needSave = True
        except AchievementReceivedError:
            pass

        return needSave

    def _checkSuitesAchievements(self, userInfo, itemInfo, achievementsList):
        needSave = False

        try:
            userAchv = None

            if itemInfo['code'] == 'purple_suite':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'mystic', userInfo, False)
            elif itemInfo['code'] == 'yellow_suite':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'Yellow-Ranger', userInfo, False)
            elif itemInfo['code'] == 'white_suite':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'white-eagle', userInfo, False)
            elif itemInfo['code'] == 'orange_suite':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'mandarin', userInfo, False)
            elif itemInfo['code'] == 'green_suite':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'raver', userInfo, False)
            elif itemInfo['code'] == 'red_suite':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'diablo', userInfo, False)
            elif itemInfo['code'] == 'black_suite':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'wild-darkness', userInfo, False)
            elif itemInfo['code'] == 'russian_vodka_developer':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'paratrooper', userInfo, False)
            elif itemInfo['code'] == 'skin_of_the_geologist':
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'geolog', userInfo, False)

            if userAchv:
                achievementsList.append(userAchv)
                needSave = True
        except AchievementReceivedError:
            pass

        return needSave

    def _checkHatsAchievements(self, userInfo, achievementsList):
        needSave = False

        try:
            userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'dendy', userInfo, False)
            if userAchv:
                achievementsList.append(userAchv)
                needSave = True
        except AchievementReceivedError:
            pass

        hatsCount = Inventory.objects.filter(user_id=userInfo.id, item__rubric=Items.RUBRIC_HATS).count()
        if hatsCount >= 4:
            try:
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'trendy', userInfo, False)
                if userAchv:
                    achievementsList.append(userAchv)
                    needSave = True
            except AchievementReceivedError:
                pass
        elif hatsCount >= 7:
            try:
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'judge', userInfo, False)
                if userAchv:
                    achievementsList.append(userAchv)
                    needSave = True
            except AchievementReceivedError:
                pass
        elif hatsCount >= 10:
            try:
                userAchv, newUserInfo = checkAchievementByCode(userInfo.id, 'esthete', userInfo, False)
                if userAchv:
                    achievementsList.append(userAchv)
                    needSave = True
            except AchievementReceivedError:
                pass

        return needSave

    def _getUpgradesCount(self):
        itemsList = ListsLogic().getItemsList()
        count = 0
        for itemId in itemsList:
            if itemsList[itemId]['rubric'] == Items.RUBRIC_UPGRADES:
                count += 1
        return count

    def _logBuyItem(self, itemId):
        try:
            logRecord = ShopStatistics.objects.get(item_id=itemId)
        except ObjectDoesNotExist:
            logRecord = ShopStatistics(item_id=itemId)
        logRecord.count_global += 1
        logRecord.count_day += 1
        logRecord.count_week += 1
        logRecord.count_month += 1
        logRecord.save()

    def equipItem(self, userId, itemId):
        if not (str(userId).isdigit() and str(itemId).isdigit()):
            raise NotCorrectError('Wrond user id or item id format')
        userId = long(userId)
        itemId = long(itemId)

        itemInfo = ListsLogic().getItemInfo(itemId)
        allRecords = Inventory.objects.filter(user_id=userId, item__rubric=itemInfo['rubric'])
        allRecords.update(equipped=0)

        responseList = {}
        inventoryRecord = None
        for record in allRecords:
            if record.item_id == itemId:
                inventoryRecord = record
            else:
                responseList[record.id] = model_to_dict(record)

        if inventoryRecord is None:
            raise NotCorrectError('Item id is invalid')

        inventoryRecord.equipped = 1
        if itemInfo['action'] == Items.ACTION_SUITE:
            inventoryRecord.is_suite = True
        inventoryRecord.save()

        responseList[inventoryRecord.id] = model_to_dict(inventoryRecord)
        cache.delDataFromCache('inventory', (userId,))
        return responseList

    def pay(self, userId, coinsCount, currency='coins'):
        """
        pay(self:object, userId:long, coinsCount:long, currency:string) -> long
        Заплатить со счета пользователя.
        Аргумент currency может принимать значения coins или gems.
        Возвращает новое количество выбранной валюты.
        """
        if not (str(userId).isdigit() and str(coinsCount).isdigit() and currency in ('coins', 'gems')):
            raise NotCorrectError('Wrong argument format')

        coinsCount = long(coinsCount)
        userInfo = Users.objects.get(id=userId)
        if currency == 'coins':
            newCoins = userInfo.coins - coinsCount
        else:
            newCoins = userInfo.gems - coinsCount

        if newCoins >= 0:
            if currency == 'coins':
                userInfo.coins = newCoins
            else:
                userInfo.gems = newCoins
            userInfo.save()
        else:
            raise NotCorrectError('Negative coins count: ' + str(newCoins))

        self.clearUserInfoCache(userInfo.soc_net_id)
        return newCoins

    def clearUserInfoCache(self, socNetId):
        cache.delDataFromCache('user_info', (socNetId,))

    @cache.cachedFunction('user_achievements')
    def getUserAchievements(self, userId):
        if not str(userId).isdigit():
            raise NotCorrectError('Wrong user id format')
        achievementsModel = UsersAchievements.objects.filter(user_id=userId)
        achievementsList = buildModelResponseStruct(achievementsModel)
        return achievementsList

    def registerUser(self, **kwargs):
        strSocNetId = str(kwargs.get('socNetId'))
        if not strSocNetId.isdigit():
            raise NotCorrectError('Wrong soc net id format')
        socNetId = long(strSocNetId)
        referrer = unicode(kwargs.get('referrer'))
        user = self.addNewUser(socNetId, referrer)
        inventory = self.giveDefaultItems(user)
        userInfo = model_to_dict(user)
        return userInfo, inventory

    def addNewUser(self, socNetId, referrer):
        if socNetId <= 0:
            raise NotCorrectError('Wrong soc net id')
        user = Users()
        user.soc_net_id = socNetId
        user.referrer = referrer
        user.register_date = datetime.datetime.utcnow()
        user.save()
        return user

    def giveDefaultItems(self, user):
        needItems = Items.objects.filter(add_during_registration=True)
        inventory = {}
        for itemData in needItems:
            invRecord = Inventory(
                user_id=user.id,
                item_id=itemData.id,
                count=1,
                equipped=1,
            )
            if itemData.action == Items.ACTION_SUITE:
                invRecord.is_suite = True
            invRecord.save()
            inventory[invRecord.id] = model_to_dict(invRecord)
        return inventory

    def exploreCapsule(self, explorerUserId, userId):
        if explorerUserId == userId:
            raise NotCorrectError('You can\'t explore your own capsule')

        explorerUserInfo = Users.objects.get(id=explorerUserId)
        userInfo = Users.objects.get(id=userId)

        utcNow = datetime.datetime.utcnow()
        nowTime = time.mktime(utcNow.timetuple())
        lastExploreTime = time.mktime(userInfo.last_explore_time.timetuple())
        if nowTime - lastExploreTime <= 86400:
            raise NotCorrectError('Too soon')
        userInfo.last_explore_time = utcNow
        userInfo.save()

        lastExploreTime = time.mktime(explorerUserInfo.last_explore_guest_time.timetuple())
        if nowTime - lastExploreTime <= 86400:
            exploredCapsules = explorerUserInfo.explored_capsules
        else:
            exploredCapsules = 0
            explorerUserInfo.last_explore_guest_time = utcNow
            explorerUserInfo.explored_capsules = 0

        if exploredCapsules >= GAME_SETTINGS['max_capsules_to_explore']:
            raise NotCorrectError('Too many')
        explorerUserInfo.explored_capsules += 1

        coinsParams = GAME_SETTINGS['explore_coins']
        coins = coinsParams['min'] + random.random() * (coinsParams['max'] - coinsParams['min'])
        coins = round(coins)
        explorerUserInfo.coins += coins
        explorerUserInfo.save()

        item = self._getRandomExploreItem()
        if item:
            inventoryRecord = self._addItemToUser(explorerUserId, item['id'], 1)
            inventoryRecord.save()
        else:
            inventoryRecord = None

        cache.delDataFromCache('user_info', (explorerUserInfo.soc_net_id,))
        cache.delDataFromCache('inventory', (explorerUserInfo.id,))
        cache.delDataFromCache('user_info', (userInfo.soc_net_id,))

        return explorerUserInfo, inventoryRecord, coins

    def _getRandomExploreItem(self):
        itemsList = ListsLogic().getItemsList()
        if not len(itemsList):
            return None

        needItems = []
        for itemId in itemsList:
            if itemsList[itemId]['action'] in (Items.ACTION_RESURRECTION, Items.ACTION_PARACHUTE):
                needItems.append(itemsList[itemId])
        maxN = len(needItems) - 1
        n = random.randint(0, maxN)
        return needItems[n]

    def giveFriendsBonuses(self, socNet, bonusType, locale='en'):
        if bonusType not in (
            self.BONUS_COINS,
            self.BONUS_GEM,
            self.BONUS_PARACHUTES,
            self.BONUS_RESURRECTIONS,
            self.BONUS_ROCKET
        ):
            raise NotCorrectError('Wrong bonuse type value: {0}'.format(bonusType))

        socNetId = socNet.getSocNetId()
        userInfo = self.getUserInfo(socNetId)
        if userInfo['was_friends_prizes_given']:
            raise NotCorrectError('Bonuses have already given')

        friendsSocNetIdsList = socNet.getFriendsList(socNetId)
        friendsModels = Users.objects.filter(soc_net_id__in=friendsSocNetIdsList)

        friendsIdsList = []
        count = 0
        if bonusType in (self.BONUS_GEM, self.BONUS_COINS):
            for model in friendsModels:
                if bonusType == self.BONUS_GEM:
                    count = GAME_SETTINGS['friends_gifts']['gems']
                    model.gems += count
                else:
                    count = GAME_SETTINGS['friends_gifts']['coins']
                    model.coins += count
                model.save()
                self.clearUserInfoCache(model.soc_net_id)
                friendsIdsList.append(model.id)
        else:
            count = GAME_SETTINGS['friends_gifts'][bonusType]
            itemInfo = ListsLogic().getItemInfoByAction(bonusType)
            for model in friendsModels:
                inventoryRecord = self._addItemToUser(model.id, itemInfo['id'], count)
                inventoryRecord.save()
                self.clearUserInfoCache(model.soc_net_id)
                friendsIdsList.append(model.id)

        userModel = Users.objects.get(id=userInfo['id'])
        userModel.was_friends_prizes_given = True
        userModel.save()
        self.clearUserInfoCache(socNetId)

        newsType = 1
        newsDataArguments = (str(socNetId), bonusType, str(count))
        newsData = '{'\
                   + '"soc_net_id":"{0}","bonus_type":"{1}","count":{2}'.format(*newsDataArguments)\
                   + '}'
        news.addUserNews(friendsIdsList, newsType, newsData)

        if hasattr(socNet, 'sendNotification'):
            if locale == 'ru':
                message = 'Друг подарил вам подарок'
            else:
                message = 'Your friend gave you a gift'
            socNet.sendNotification(friendsSocNetIdsList, message.decode('UTF-8'))

        return userModel
