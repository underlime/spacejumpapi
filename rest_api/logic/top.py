import datetime
from django.forms import model_to_dict
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.logic import Logic
from rest_api.models import Users, Inventory
from rest_api import cache

__author__ = 'Andre'


class TopLogic(Logic):
    TOP_GLOBAL = 'global'
    TOP_MONTH = 'month'
    TOP_WEEK = 'week'
    TOP_DAY = 'day'

    @cache.cachedFunction('top', 300)
    def getTop(self, topType):
        usersModelData = self._getTopData(topType)
        if topType == self.TOP_DAY:
            self._setPlaceInTop(usersModelData)
        usersListData, userIdsList = self._buildUsersList(usersModelData)
        inventoriesData = self._getRelatedInventories(userIdsList)
        return usersListData, inventoriesData

    def _getTopData(self, topType):
        if topType == self.TOP_GLOBAL:
            orderType = '-max_score_global'
            filterCondition = {
                'max_score_global__gt': 0
            }
        elif topType == self.TOP_MONTH:
            orderType = '-max_score_month'
            filterCondition = {
                'max_score_month__gt': 0
            }
        elif topType == self.TOP_WEEK:
            orderType = '-max_score_week'
            filterCondition = {
                'max_score_week__gt': 0
            }
        elif topType == self.TOP_DAY:
            orderType = '-max_score_day'
            filterCondition = {
                'max_score_day__gt': 0
            }
        else:
            raise NotCorrectError('Wrong type')
        return Users.objects.filter(**filterCondition).order_by(orderType)[0:50]

    def _setPlaceInTop(self, usersModelData):
        date = datetime.datetime.utcnow()
        strDate = date.strftime('%Y-%m-%d %H:%M:%S')
        counter = 1
        for userData in usersModelData:
            userData.place_in_top = counter
            userData.place_in_top_time = strDate
            userData.save()
            counter += 1
            cache.delDataFromCache('user_info', (userData.soc_net_id,))

    def _buildUsersList(self, usersModelData):
        usersList = []
        userIdsList = []
        for record in usersModelData:
            usersList.append(model_to_dict(record))
            userIdsList.append(record.id)
        return usersList, userIdsList

    def _getRelatedInventories(self, userIdsList):
        inventoriesData = {}
        for listId in userIdsList:
            inventoriesData[listId] = []
        modelCollection = Inventory.objects.filter(user__in=userIdsList, equipped=1)
        for record in modelCollection:
            inventoriesData[record.user_id].append(model_to_dict(record))
        return inventoriesData
