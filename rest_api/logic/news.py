import datetime
from django.db import models as django_models
from django.forms import model_to_dict
from rest_api import models, cache
from rest_api.logic.exceptions import NotCorrectError
from rest_api.models import Users
from spacejump import settings


__author__ = 'Andre'


@cache.cachedFunction('user_news')
def getUserNews(userId):
    filterArgs = django_models.Q(user_id=userId) | django_models.Q(user_id__isnull=True)
    modelData = models.UserNews.objects.filter(filterArgs).order_by('-time')
    newsList = {}
    for model in modelData:
        newsList[model.id] = model_to_dict(model)
    return newsList


def addUserNews(userIds, newsType, additionalData='', liveSeconds=604800):
    if not str(newsType).isdigit():
        raise NotCorrectError('Wrong news type format')
    newsType = long(newsType)
    if not (1 <= newsType <= models.UserNews.MAX_NEWS_TYPE):
        raise NotCorrectError('Wrong news type value')

    if not str(liveSeconds).isdigit():
        raise NotCorrectError('Wrong live seconds format')
    liveSeconds = long(liveSeconds)

    if isinstance(userIds, list):
        for userId in userIds:
            _addOneNews(userId, newsType, additionalData, liveSeconds)
    else:
        _addOneNews(userIds, newsType, additionalData, liveSeconds)


def _addOneNews(userId, newsType, additionalData, liveSeconds):
        expiresTime = datetime.datetime.utcnow() + datetime.timedelta(seconds=liveSeconds)
        newsRecord = models.UserNews(
            user_id=userId,
            news_type=newsType,
            time=datetime.datetime.utcnow(),
            expires_time=expiresTime,
            additional_data=additionalData,
        )
        newsRecord.save()
        cache.delDataFromCache('user_news', (userId,))


@cache.cachedFunction('user_news_read')
def getNewsRead(userId):
    newsReadModels = models.UserNewsRead.objects.filter(user_id=userId)
    newsReadDict = {}
    for newsReadModel in newsReadModels:
        newsReadDict[newsReadModel.id] = model_to_dict(newsReadModel)
    return newsReadDict


def setNewsRead(userId, newsId, isShared):
    count = models.UserNewsRead.objects.filter(user_id=userId, news_id=newsId).count()
    if count > 0:
        raise NotCorrectError('News was already read')

    newsRead = models.UserNewsRead(
        user_id=userId,
        news_id=newsId,
        shared=isShared
    )
    newsRead.save()

    cache.delDataFromCache('user_news_read', (userId,))

    userInfo = None
    if isShared:
        userInfo = Users.objects.get(id=userId)
        userInfo.coins += settings.GAME_SETTINGS['news_like_coins']
        userInfo.save()
        cache.delDataFromCache('user_info', (userInfo.soc_net_id,))

    return userInfo
