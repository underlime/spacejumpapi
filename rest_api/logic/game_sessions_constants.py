from rest_api.models import GameSessions


__author__ = 'Andre'

TROPHIES_IN_COLLECTION = 5
MAX_TIME_ERROR = 25
MAX_VELOCITY = 1010

SIG_FIELDS_LIST = ('session_id', 'win', 'score', 'bronze_coins', 'silver_coins', 'gold_coins', 'coins',
                   'trophy_found', 'cola', 'distance', 'booster_count', 'power_shield_count',
                   'heart_count', 'shield_count', 'max_velocity', 'gems_resurrections_count', 'gems_spent',
                   'physics_tics', 'real_time', 'pause_seconds', 'bonus_seconds', 'seconds',)

DISTANCES_TABLE = {
    GameSessions.DIFFICULTY.EASY: 40000,
    GameSessions.DIFFICULTY.MEDIUM: 70000,
    GameSessions.DIFFICULTY.HARD: 100000,
}

DIFFICULTIES_TABLE = {
    GameSessions.DIFFICULTY.EASY: 1,
    GameSessions.DIFFICULTY.MEDIUM: 2,
    GameSessions.DIFFICULTY.HARD: 3,
    GameSessions.DIFFICULTY.ENDLESS: 4,
}
