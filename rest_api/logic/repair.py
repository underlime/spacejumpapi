import time
import datetime
from django.core.exceptions import ObjectDoesNotExist
from rest_api.logic.achievements import checkAchievementByCode, AchievementReceivedError
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.lists import ListsLogic
from rest_api.logic.user import UserLogic
from rest_api.models import Inventory
from spacejump.settings import GAME_SETTINGS

__author__ = 'Andre'


def getSuiteParams(userId):
    try:
        suiteRecord = Inventory.objects.get(user_id=userId, is_suite=True, equipped=1)
    except ObjectDoesNotExist:
        raise Exception('Suite not found')
    hpTimeStamp = time.mktime(suiteRecord.hp_time.timetuple())
    itemInfo = ListsLogic().getItemInfo(suiteRecord.item_id)
    if (suiteRecord.hp < itemInfo['max_hp']) and \
            (time.mktime(datetime.datetime.utcnow().timetuple())
             - hpTimeStamp >= GAME_SETTINGS['suite_damage']['auto_repair_seconds']):
        suiteRecord.hp = itemInfo['max_hp']
        suiteRecord.hp_time = datetime.datetime.utcnow()
        suiteRecord.save()
    return suiteRecord, itemInfo


def autoRepairSuite(userId, difficulty):
    suiteRecord, itemInfo = getSuiteParams(userId)
    repairLevel = GAME_SETTINGS['suite_damage']['restore_by_win'][difficulty]
    if repairLevel == 0:
        return suiteRecord

    if suiteRecord.hp >= itemInfo['max_hp']:
        return suiteRecord

    suiteRecord.hp += round(itemInfo['max_hp'] * repairLevel)
    if suiteRecord.hp > itemInfo['max_hp']:
        suiteRecord.hp = itemInfo['max_hp']
    suiteRecord.save()

    return suiteRecord


def setSuiteDamage(userId):
    suiteRecord, itemInfo = getSuiteParams(userId)
    if not GAME_SETTINGS['suite_damage']['enabled_damage']:
        return suiteRecord

    if suiteRecord.hp == itemInfo['max_hp']:
        suiteRecord.hp_time = datetime.datetime.utcnow()

    if suiteRecord.hp > 0:
        suiteRecord.hp -= 1

    suiteRecord.save()

    return suiteRecord


def repairSuite(userId, hpPoints, currency):
    if currency not in ('coins', 'gems'):
        raise NotCorrectError('Wring currency')

    suiteParams, itemInfo = getSuiteParams(userId)
    damagePoints = itemInfo['max_hp'] - suiteParams.hp
    if not damagePoints:
        return suiteParams

    if currency == 'coins':
        realHpPoints = min(hpPoints, damagePoints)
        repairPrice = realHpPoints * GAME_SETTINGS['suite_damage']['repair_price']
        newCoins = UserLogic().pay(userId, repairPrice, 'coins')
        suiteParams.hp += realHpPoints
    else:
        newCoins = UserLogic().pay(userId, 1, 'gems')
        suiteParams.hp = itemInfo['max_hp']
    suiteParams.save()

    achv = None
    userInfo = None
    try:
        achv, userInfo = checkAchievementByCode(userId, 'mechanic')
    except AchievementReceivedError:
        pass

    return suiteParams, newCoins, achv, userInfo
