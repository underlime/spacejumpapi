from django.core.exceptions import ObjectDoesNotExist
from rest_api import cache
from rest_api.logic.exceptions import NotCorrectError
from rest_api.models import UsersAchievements, Achievements, Users

__author__ = 'Andre'


class AchievementReceivedError(NotCorrectError):
    pass


def checkAchievementByCode(userId, code, userInfo=None, userSave=True):
    try:
        achv = Achievements.objects.get(code=code)
    except ObjectDoesNotExist:
        return None, None

    try:
        UsersAchievements.objects.get(user_id=userId, achievement_id=achv.id)
        raise AchievementReceivedError('Achievement is already received')
    except ObjectDoesNotExist:
        pass

    userAchv = UsersAchievements(user_id=userId, achievement_id=achv.id)
    userAchv.save()

    cache.delDataFromCache('user_achievements', (userId,))

    if userInfo is None:
        userInfo = Users.objects.get(id=userId)
    userInfo.stars += 1
    if userSave:
        userInfo.save()

    cache.delDataFromCache('user_info', (userInfo.soc_net_id,))

    return userAchv, userInfo
