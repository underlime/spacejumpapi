from django.conf import settings
from rest_api import cache
from rest_api.logic.exceptions import NotCorrectError
from rest_api.models import Users


def rewardUser(socNetId, prizeType):
    if prizeType not in ('favorites', 'group'):
        raise NotCorrectError('Unknown prize type')
    currentPrize = _getCurrentPrize(prizeType)
    userRecord = Users.objects.get(soc_net_id=socNetId)
    _checkPrizeAlreadyGiven(userRecord, prizeType)
    userRecord.coins += currentPrize
    userRecord.save()
    cache.delDataFromCache('user_info', (socNetId,))
    return userRecord


def _getCurrentPrize(prizeType):
    prizes = settings.GAME_SETTINGS['soc_net']['vk_com']['actions_prizes']
    currentPrize = prizes.get(prizeType)
    if not currentPrize:
        raise NotCorrectError('Prize type is invalid')
    return currentPrize


def _checkPrizeAlreadyGiven(userRecord, prizeType):
    if prizeType == 'favorites':
        if userRecord.favorites_prize:
            raise NotCorrectError('Favorites prize already given')
        userRecord.favorites_prize = True

    if prizeType == 'group':
        if userRecord.join_group_prize:
            raise NotCorrectError('Group prize already given')
        userRecord.join_group_prize = True
