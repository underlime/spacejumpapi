__author__ = 'Andre'


class NotCorrectError(Exception):
    pass


class UserDoesNotExist(Exception):
    pass


class SocNetAuthError(Exception):
    pass


class SocNetParamsError(Exception):
    pass


class SocNetRequestError(Exception):
    pass
