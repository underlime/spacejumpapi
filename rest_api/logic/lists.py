from django.core.exceptions import ObjectDoesNotExist
from rest_api import cache
from rest_api.logic.exceptions import NotCorrectError
from rest_api.logic.logic import Logic
from rest_api.models import Items, Achievements
from rest_api.utils import buildModelResponseStruct

__author__ = 'Andre'


class ListsLogic(Logic):
    @cache.cachedFunction('items_list')
    def getItemsList(self):
        itemsData = Items.objects.all()
        itemsList = buildModelResponseStruct(itemsData)
        return itemsList

    @cache.cachedFunction('item_info_by_id')
    def getItemInfo(self, itemId):
        if not str(itemId).isdigit():
            raise NotCorrectError('Wrong item id')
        itemId = long(itemId)
        itemsList = self.getItemsList()
        itemInfo = itemsList.get(itemId)
        if itemInfo is None:
            raise ObjectDoesNotExist('Item does not exists')
        return itemInfo

    @cache.cachedFunction('item_info_by_action')
    def getItemInfoByAction(self, actionName):
        itemsList = self.getItemsList()
        itemInfo = None
        for itemId in itemsList:
            if itemsList[itemId]['action'] == actionName:
                itemInfo = itemsList[itemId]
                break
        if itemInfo is None:
            raise ObjectDoesNotExist('Item does not exists')
        return itemInfo

    @cache.cachedFunction('achievements_list')
    def getAchievementsList(self):
        achievementsData = Achievements.objects.all()
        achievementsList = buildModelResponseStruct(achievementsData)
        return achievementsList
