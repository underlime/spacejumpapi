# coding=utf-8
from abc import abstractmethod, ABCMeta
import base64
import hashlib
import hmac
from httplib import HTTPConnection, HTTPException
import json
import random
import datetime
from time import mktime
import urllib
import urlparse
import time

import facebook

from spacejump import settings
from rest_api.logic.exceptions import SocNetParamsError, SocNetAuthError, SocNetRequestError
from spacejump import settings_control_sets
from spacejump.db_routers import DbRouter


__author__ = 'Andre'


class SocNet(object):
    """
    Базовый класс и фабрика объектов для взаимодействия
    с соц. сетями
    """
    __metaclass__ = ABCMeta
    _request = None

    @classmethod
    def factory(cls, request):
        """
        factory(request:dict) -> SocNet
        """
        if DbRouter.currentDb == DbRouter.VK_COM:
            return VkComSocNet(request)
        elif DbRouter.currentDb == DbRouter.FACEBOOK_COM:
            return FacebookComSocNet(request)
        elif DbRouter.currentDb == DbRouter.ODNOKLASSNIKI_RU:
            return OdnoklassnikiRuSocNet(request)
        elif DbRouter.currentDb == DbRouter.MY_MAIL_RU:
            return MyMailRuSocNet(request)
        else:
            raise SocNetParamsError('Wrong social network type')

    def __init__(self, request, **kwargs):
        self._request = request
        super(SocNet, self).__init__()

    @abstractmethod
    def makeToken(self, **kwargs):
        pass

    @abstractmethod
    def checkAuthParams(self):
        pass

    @abstractmethod
    def getSocNetId(self):
        pass

    @abstractmethod
    def getSocNetName(self):
        pass

    @abstractmethod
    def getFriendsList(self, socNetId):
        pass

    def setUserLevel(self, socNetId, level):
        pass


class VkComSocNet(SocNet):
    API_SERVER = 'api.vk.com'
    API_URL = 'http://api.vk.com/api.php'

    def __init__(self, request, **kwargs):
        if request:
            self.__setAuthParams(request, **kwargs)
        super(VkComSocNet, self).__init__(request, **kwargs)

    def __setAuthParams(self, request, authKey=None, viewerId=None, sid=None):
        self._authKey = request.POST.get('auth_key', authKey)
        self._viewerId = request.POST.get('viewer_id', viewerId)
        self._sid = request.POST.get('sid', sid)
        if not self._authKey or not self._viewerId or not self._sid:
            raise SocNetParamsError('Auth params need')

    def checkAuthParams(self):
        rightAuthKey = self.makeAuthKey(self._viewerId)
        if not self._viewerId or not self._sid or (self._authKey != rightAuthKey):
            raise SocNetAuthError('Auth key is invalid')

    def makeToken(self, mockRequestUri=None):
        salt = settings.GAME_SETTINGS['token_salt']
        if mockRequestUri:
            requestUri = mockRequestUri
        else:
            requestUri = self._request.META.get('PATH_INFO')

        if requestUri[-1] != '/':
            requestUri += '/'

        baseStringParams = {
            'viewer_id': self._viewerId,
            'auth_key': self._authKey,
            'sid': self._sid,
            'request_uri': requestUri,
            'salt': salt,
        }
        baseString = '{auth_key}{sid}{viewer_id}{request_uri}{salt}'.format(**baseStringParams)
        return hashlib.md5(baseString).hexdigest()

    @classmethod
    def makeAuthKey(cls, viewerId):
        baseStringParams = {
            'api_id': settings.GAME_SETTINGS['soc_net']['vk_com']['api_id'],
            'viewer_id': viewerId,
            'api_secret': settings.GAME_SETTINGS['soc_net']['vk_com']['api_secret'],
        }
        baseString = '{api_id}_{viewer_id}_{api_secret}'.format(**baseStringParams)
        return hashlib.md5(baseString).hexdigest()

    def getSocNetId(self):
        return self._viewerId

    def getFriendsList(self, socNetId):
        if settings_control_sets.CURRENT_CONTROL_SET == 'localhost' and settings.FRIENDS_DEBUG:
            return [3081515, 173423640, 179371856, 125852482, 13950925, 79469918]

        params = {
            'user_id': socNetId,
            'fields': 'uid',
        }
        data = self._makeApiCall('friends.get', params)
        response = data.get('data', {}).get('response', {})
        friendsList = list((long(row['user']['uid']) for row in response))
        return friendsList

    def setUserLevel(self, socNetId, level):
        params = {
            'uid': socNetId,
            'level': level,
        }
        try:
            return self._makeApiCall('secure.setUserLevel', params)
        except SocNetRequestError:
            pass
        return None

    def sendNotification(self, socNetIdsList, message):
        params = {
            'uids': ','.join((str(val) for val in socNetIdsList)),
            'message': message,
        }
        try:
            return self._makeApiCall('secure.sendNotification', params)
        except SocNetRequestError:
            pass
        return None

    def _makeApiCall(self, method, params=None):
        rand = random.randint(1, 1000)
        time = mktime(datetime.datetime.now().timetuple())
        time = long(time)

        if params is None:
            params = {}

        params.update({
            'api_id': settings.GAME_SETTINGS['soc_net']['vk_com']['api_id'],
            'format': 'json',
            'method': method,
            'rand': rand,
            'timestamp': time,
        })

        if hasattr(self, '_sid') and self._sid:
            params['sid'] = self._sid

        strParams = {}
        for key in params:
            strParams[key] = unicode(params[key]).encode('UTF-8')

        secretString = ''
        for paramName in sorted(params.iterkeys()):
            secretString += paramName + '=' + strParams[paramName]
        secretString += settings.GAME_SETTINGS['soc_net']['vk_com']['api_secret']
        sig = hashlib.md5(secretString).hexdigest()

        requestUrl = self.API_URL + '?sig=' + sig
        requestUrl += '&' + urllib.urlencode(strParams)

        connection = HTTPConnection(self.API_SERVER)
        try:
            connection.connect()
            connection.request('GET', requestUrl)
            response = connection.getresponse()
            httpStatus = response.status
            responseText = response.read()
            response.close()
            connection.close()
        except HTTPException:
            raise SocNetRequestError()
        finally:
            connection.close()

        try:
            responseDecoded = json.loads(responseText, encoding='UTF-8')
        except ValueError:
            responseDecoded = {}

        return {
            'status': httpStatus,
            'data': responseDecoded,
        }

    def getSocNetName(self):
        return 'vk_com'


class FacebookComSocNet(SocNet):
    def __init__(self, request, **kwargs):
        self.__setAuthParams(request, **kwargs)
        super(FacebookComSocNet, self).__init__(request, **kwargs)

    def __setAuthParams(self, request, signed_request=None, access_token=None):
        self._accessToken = request.POST.get('access_token', access_token)
        self._signedRequest = request.POST.get('signed_request', signed_request)
        if (not self._signedRequest) or (not self._accessToken):
            raise SocNetParamsError('Auth params need')
        self.__extractSignedRequestData()

    def __extractSignedRequestData(self):
        encodedSig, payload = self._signedRequest.split('.', 2)

        strPayload = str(payload)
        jsonData = base64.urlsafe_b64decode(strPayload+'==')
        try:
            self._requestData = json.loads(jsonData)
            assert(self._requestData['algorithm'].upper() == 'HMAC-SHA256')
        except (ValueError, AssertionError):
            raise SocNetParamsError('Wrong signed request')

        strEncodedSig = str(encodedSig)
        self._sig = base64.urlsafe_b64decode(strEncodedSig+'==')
        self._userId = self._requestData['user_id']
        
        secret = settings.GAME_SETTINGS['soc_net']['facebook_com']['api_secret']
        self._rightSig = hmac.new(secret, msg=strPayload, digestmod=hashlib.sha256).digest()

    def checkAuthParams(self):
        if not self._userId or (self._sig != self._rightSig):
            raise SocNetAuthError('Signed request is invalid')

    def makeToken(self):
        salt = settings.GAME_SETTINGS['token_salt']
        requestUri = self._request.META.get('PATH_INFO')
        if requestUri[-1] != '/':
            requestUri += '/'
        baseStringParams = {
            'signed_request': self._signedRequest,
            'request_uri': requestUri,
            'salt': salt,
        }
        baseString = '{signed_request}{request_uri}{salt}'.format(**baseStringParams)
        return hashlib.md5(baseString).hexdigest()

    def getFriendsList(self, socNetId):
        api = facebook.GraphAPI(self._accessToken)
        requestUserId = str(socNetId)
        friends = api.get_connections(requestUserId, 'friends')
        friendsList = list((long(friend['id']) for friend in friends['data']))
        return friendsList

    def getSocNetId(self):
        return long(self._userId)

    def getSocNetName(self):
        return 'facebook_com'


class OdnoklassnikiRuSocNet(SocNet):
    def __init__(self, request, **kwargs):
        self.__setAuthParams(request, **kwargs)
        super(OdnoklassnikiRuSocNet, self).__init__(request, **kwargs)

    def __setAuthParams(self, request, queryParams=None):
        self._queryParamsJson = request.POST.get('query_params', queryParams)
        if not self._queryParamsJson:
            raise SocNetParamsError('Auth params need')
        self.__extractQueryParamsData()

    def __extractQueryParamsData(self):
        try:
            self._queryParams = json.loads(self._queryParamsJson)
        except ValueError:
            raise SocNetParamsError('Wrong query params')
        self._userId = self._queryParams.get('logged_user_id')
        self._sig = self._queryParams.get('sig')
        self._apiServer = self._queryParams.get('api_server', 'http://api.odnoklassniki.ru/')
        self._applicationKey = self._queryParams.get('application_key')
        self._sessionKey = self._queryParams.get('session_key')

    def checkAuthParams(self):
        rightSig = self._makeSignature(self._queryParams)
        if not self._userId or (self._sig != rightSig):
            raise SocNetAuthError('Signature is invalid')

    def _makeSignature(self, queryParams):
        rightSigBase = ''
        keysList = sorted(queryParams.iterkeys())
        for key in keysList:
            if key != 'sig':
                rightSigBase += key + '=' + queryParams[key]
        rightSigBase += settings.GAME_SETTINGS['soc_net']['odnoklassniki_ru']['api_secret']
        rightSig = hashlib.md5(rightSigBase).hexdigest()
        return rightSig

    def makeToken(self):
        salt = settings.GAME_SETTINGS['token_salt']
        requestUri = self._request.META.get('PATH_INFO')
        if requestUri[-1] != '/':
            requestUri += '/'
        baseStringParams = {
            'query_params': self._queryParamsJson,
            'request_uri': requestUri,
            'salt': salt,
        }
        baseString = '{query_params}{request_uri}{salt}'.format(**baseStringParams)
        return hashlib.md5(baseString).hexdigest()

    def getFriendsList(self, socNetId):
        data = self._makeApiCall('friends.get', {'uid': socNetId})
        return list((long(uid) for uid in data['data']))

    def sendNotification(self, socNetIdsList, message):
        params = {'text': message}
        results = []
        needSleep = (len(socNetIdsList) > 1)
        for socNetId in socNetIdsList:
            params['uid'] = socNetId
            try:
                res = self._makeApiCall('notifications.sendSimple', params)
                results.append(res)
                if needSleep:
                    time.sleep(0.35)
            except SocNetRequestError:
                pass
        return results

    def _makeApiCall(self, method, params=None):
        if params is None:
            params = {}

        params['method'] = method
        params['application_key'] = self._applicationKey
        params['format'] = 'JSON'
        strParams = {}
        for key in params:
            strParams[key] = unicode(params[key]).encode('UTF-8')

        sig = self._makeSignature(strParams)
        strParams['sig'] = sig

        requestUrl = self._apiServer + 'fb.do?'
        requestUrl += urllib.urlencode(strParams)

        urlParts = urlparse.urlparse(self._apiServer)
        connection = HTTPConnection(urlParts.netloc)
        try:
            connection.connect()
            connection.request('GET', requestUrl)
            response = connection.getresponse()
            httpStatus = response.status
            responseText = response.read()
            response.close()
            connection.close()
        except HTTPException:
            raise SocNetRequestError()
        finally:
            connection.close()

        try:
            responseDecoded = json.loads(responseText, encoding='UTF-8')
        except ValueError:
            responseDecoded = {}

        if isinstance(responseDecoded, dict):
            errorMsg = responseDecoded.get('error_msg')
            if errorMsg:
                raise SocNetRequestError(errorMsg)

        return {
            'status': httpStatus,
            'data': responseDecoded,
        }

    def getSocNetId(self):
        return self._userId

    def getSocNetName(self):
        return 'odnoklassniki_ru'


class MyMailRuSocNet(SocNet):
    API_SERVER = 'www.appsmail.ru'
    API_URL = 'http://www.appsmail.ru/platform/api'

    def __init__(self, request, **kwargs):
        self.__setAuthParams(request, **kwargs)
        super(MyMailRuSocNet, self).__init__(request, **kwargs)

    def __setAuthParams(self, request, queryParams=None):
        self._queryParamsJson = request.POST.get('query_params', queryParams)
        if not self._queryParamsJson:
            raise SocNetParamsError('Auth params need')
        self.__extractQueryParamsData()

    def __extractQueryParamsData(self):
        try:
            self._queryParams = json.loads(self._queryParamsJson)
        except ValueError:
            raise SocNetParamsError('Wrong query params')
        self._userId = self._queryParams.get('vid')
        self._sig = self._queryParams.get('sig')
        self._sessionKey = self._queryParams.get('session_key')

    def checkAuthParams(self):
        rightSig = self._makeSignature(self._queryParams)
        if not self._userId or (self._sig != rightSig):
            raise SocNetAuthError('Signature is invalid')

    def _makeSignature(self, queryParams):
        rightSigBase = ''
        keysList = sorted(queryParams.iterkeys())
        for key in keysList:
            if key != 'sig':
                rightSigBase += key + '=' + queryParams[key]
        rightSigBase += settings.GAME_SETTINGS['soc_net']['my_mail_ru']['api_secret']
        rightSig = hashlib.md5(rightSigBase).hexdigest()
        return rightSig

    def makeToken(self):
        salt = settings.GAME_SETTINGS['token_salt']
        requestUri = self._request.META.get('PATH_INFO')
        if requestUri[-1] != '/':
            requestUri += '/'
        baseStringParams = {
            'query_params': self._queryParamsJson,
            'request_uri': requestUri,
            'salt': salt,
        }
        baseString = '{query_params}{request_uri}{salt}'.format(**baseStringParams)
        return hashlib.md5(baseString).hexdigest()

    def getFriendsList(self, socNetId):
        data = self._makeApiCall('friends.get')
        return list((long(uid) for uid in data['data']))

    def sendNotification(self, socNetIdsList, message):
        params = {'text': message}
        results = []
        needSleep = (len(socNetIdsList) > 1)
        for socNetId in socNetIdsList:
            params['uids'] = socNetId
            try:
                res = self._makeApiCall('notifications.send', params)
                results.append(res)
                if needSleep:
                    time.sleep(0.35)
            except SocNetRequestError:
                pass
        return results

    def _makeApiCall(self, method, params=None):
        if params is None:
            params = {}

        params['method'] = method
        params['app_id'] = settings.GAME_SETTINGS['soc_net']['my_mail_ru']['api_id']
        params['session_key'] = self._sessionKey
        params['secure'] = '1'
        params['format'] = 'json'
        strParams = {}
        for key in params:
            strParams[key] = unicode(params[key]).encode('UTF-8')

        sig = self._makeSignature(strParams)
        strParams['sig'] = sig

        requestBody = urllib.urlencode(strParams)
        connection = HTTPConnection(self.API_SERVER)
        headers = {
            'Content-type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        }

        try:
            connection.connect()
            connection.request('POST', self.API_URL, requestBody, headers)
            response = connection.getresponse()
            httpStatus = response.status
            responseText = response.read()
            response.close()
            connection.close()
        except HTTPException:
            raise SocNetRequestError()
        finally:
            connection.close()

        try:
            responseDecoded = json.loads(responseText, encoding='UTF-8')
        except ValueError:
            responseDecoded = {}

        if isinstance(responseDecoded, dict):
            errorMsg = responseDecoded.get('error', {}).get('error_msg')
            if errorMsg:
                raise SocNetRequestError(errorMsg)

        return {
            'status': httpStatus,
            'data': responseDecoded,
        }

    def getSocNetId(self):
        return self._userId

    def getSocNetName(self):
        return 'my_mail_ru'
