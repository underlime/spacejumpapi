# coding=utf-8
import datetime
from django.core import validators
from django.db import models


class Users(models.Model):
    soc_net_id = models.CharField(
        verbose_name=u'id в соц. сети',
        max_length=32,
        blank=False,
        db_index=True,
        unique=True,
        validators=[validators.RegexValidator(
            r'^[0-9]+$',
            'Only 0-9 are allowed.',
            'Invalid soc_net_id'
        )]
    )
    register_date = models.DateTimeField(
        verbose_name=u'дата регистрации',
        blank=True,
        null=True,
        default=None,
    )
    referrer = models.CharField(
        verbose_name=u'тип реферера',
        max_length=32,
        blank=True,
        null=True,
        default=None,
        db_index=True,
    )
    photo_album_id = models.BigIntegerField(
        verbose_name=u'id фотоальбома',
        blank=True,
        null=True,
        default=None,
    )
    max_score_global = models.IntegerField(
        verbose_name=u'максимальные очки за все время',
        blank=True,
        default=0,
        db_index=True,
    )
    max_score_day = models.IntegerField(
        verbose_name=u'максимальные очки за день',
        blank=True,
        default=0,
        db_index=True,
    )
    max_score_week = models.IntegerField(
        verbose_name=u'максимальные очки за неделю',
        blank=True,
        default=0,
        db_index=True,
    )
    max_score_month = models.IntegerField(
        verbose_name=u'максимальные очки за месяц',
        blank=True,
        default=0,
        db_index=True,
    )
    jumps_count = models.IntegerField(
        verbose_name=u'количество прыжков',
        blank=True,
        default=0,
    )
    landings_count = models.IntegerField(
        verbose_name=u'количество приземлений',
        blank=True,
        default=0,
    )
    summary_time = models.IntegerField(
        verbose_name=u'время в игре',
        blank=True,
        default=0,
    )
    coins = models.IntegerField(
        verbose_name=u'монеты',
        blank=True,
        default=0,
    )
    gems = models.IntegerField(
        verbose_name=u'гемы',
        blank=True,
        default=0,
    )
    stars = models.IntegerField(
        verbose_name=u'звезды',
        blank=True,
        default=0,
    )
    health = models.IntegerField(
        verbose_name=u'доп. жизни',
        blank=True,
        default=0,
    )
    acceleration = models.IntegerField(
        verbose_name=u'ускорение',
        blank=True,
        default=0,
    )
    immortality = models.IntegerField(
        verbose_name=u'бессмертие',
        blank=True,
        default=0,
    )
    coins_improve_level = models.IntegerField(
        verbose_name=u'уровень улучшения монет',
        blank=True,
        default=0,
    )
    place_in_top = models.IntegerField(
        verbose_name=u'место в топе',
        blank=True,
        null=True,
        default=None,
    )
    place_in_top_time = models.DateTimeField(
        verbose_name=u'время обновления места в топе',
        blank=True,
        null=True,
        default=None,
    )
    favorites_prize = models.BooleanField(
        verbose_name=u'выдан приз за избранное',
        blank=True,
        default=False,
    )
    join_group_prize = models.BooleanField(
        verbose_name=u'выдан приз за вступление в группу',
        blank=True,
        default=False,
    )
    explored_capsules = models.IntegerField(
        verbose_name=u'капсул просмотрено',
        blank=True,
        default=0,
    )
    last_explore_guest_time = models.DateTimeField(
        verbose_name=u'время последнего просмотра чужой капсулы',
        blank=True,
        default=datetime.datetime(1988, 1, 1, 11, 40),
    )
    last_explore_time = models.DateTimeField(
        verbose_name=u'время последнего просмотра капсулы пользователя',
        blank=True,
        default=datetime.datetime(1988, 1, 1, 11, 40),
    )
    visits_count = models.IntegerField(
        verbose_name=u'количество посещений',
        blank=True,
        default=0,
    )
    last_visit_date = models.DateField(
        verbose_name=u'время последнего посещения',
        blank=True,
        default=datetime.datetime(1988, 1, 1),
    )
    was_friends_prizes_given = models.BooleanField(
        verbose_name=u'были розданы бонусы друзьям',
        blank=True,
        default=False,
    )

    def __unicode__(self):
        socNetId = unicode(self.soc_net_id)
        return socNetId

    class Meta(object):
        verbose_name = u'пользователь'
        verbose_name_plural = u'пользователи'


class Items(models.Model):
    UPGRADES_STADIES_COUNT = 5

    RUBRIC_UPGRADES = 'upgrades'
    RUBRIC_CONSUMABLES = 'consumables'
    RUBRIC_SUITS = 'suits'
    RUBRIC_HATS = 'hats'
    RUBRIC_CAPSULE = 'capsule'

    ACTION_HEALTH = 'improve_health'
    ACTION_ACCELERATION = 'improve_acceleration'
    ACTION_IMMORTALITY = 'improve_immortality'
    ACTION_IMPROVE_COINS = 'improve_coins'
    ACTION_PARACHUTE = 'parachute'
    ACTION_RESURRECTION = 'resurrection'
    ACTION_RESURRECTION_PLUS = 'resurrection_plus'
    ACTION_ROCKET = 'rocket'
    ACTION_MEGA_ROCKET = 'mega_rocket'
    ACTION_SUITE = 'suite'
    ACTION_HAT = 'hat'
    ACTION_CROWN = 'crown'
    ACTION_CAPSULE = 'capsule'

    RUBRICS_LIST = (
        (RUBRIC_UPGRADES, u'Апгрейды'),
        (RUBRIC_CONSUMABLES, u'Расходники'),
        (RUBRIC_SUITS, u'Скафандры'),
        (RUBRIC_HATS, u'Шапки'),
        (RUBRIC_CAPSULE, u'Капсулы'),
    )
    ACTIONS_LIST = (
        (ACTION_HEALTH, u'Здоровье'),
        (ACTION_ACCELERATION, u'Ускорение'),
        (ACTION_IMMORTALITY, u'Бессмертие'),
        (ACTION_IMPROVE_COINS, u'Улучшить монеты'),
        (ACTION_PARACHUTE, u'Парашют'),
        (ACTION_RESURRECTION, u'Воскрешение'),
        (ACTION_RESURRECTION_PLUS, u'Воскрешение плюс'),
        (ACTION_ROCKET, u'Ракета'),
        (ACTION_MEGA_ROCKET, u'Мега-ракета'),
        (ACTION_SUITE, u'Скафандр'),
        (ACTION_HAT, u'Шапка'),
        (ACTION_CROWN, u'Корона'),
        (ACTION_CAPSULE, u'Капсула'),
    )

    code = models.CharField(
        verbose_name=u'код доступа',
        max_length=32,
        db_index=True,
        blank=False,
        unique=True,
        null=True,
        default=None,
    )
    sort = models.IntegerField(
        verbose_name=u'сортировка',
        db_index=True,
        blank=False,
        default=0,
    )
    rubric = models.CharField(
        verbose_name=u'рубрика',
        max_length=32,
        db_index=True,
        choices=RUBRICS_LIST,
        blank=False,
    )
    name_ru = models.CharField(
        verbose_name=u'название (RU)',
        max_length=48,
        blank=False,
        null=True,
        default='',
    )
    name_en = models.CharField(
        verbose_name=u'название (EN)',
        max_length=48,
        blank=False,
        null=True,
        default='',
    )
    description_ru = models.CharField(
        verbose_name=u'описание (RU)',
        max_length=512,
        blank=True,
        null=True,
        default='',
    )
    description_en = models.CharField(
        verbose_name=u'описание (EN)',
        max_length=512,
        blank=True,
        null=True,
        default='',
    )
    image = models.CharField(
        verbose_name=u'image',
        max_length=32,
        blank=False,
        default='',
    )
    symbol = models.CharField(
        verbose_name=u'symbol',
        max_length=32,
        blank=False,
        default='',
    )
    action = models.CharField(
        verbose_name=u'действие',
        max_length=32,
        blank=False,
        choices=ACTIONS_LIST,
    )
    price = models.IntegerField(
        verbose_name=u'цена в монетах',
        blank=False,
        default=100,
    )
    gems_price = models.IntegerField(
        verbose_name=u'цена в гемах',
        blank=False,
        default=1,
    )
    upgrade_price = models.CharField(
        verbose_name=u'доп. цены в монетах',
        max_length=64,
        blank=True,
    )
    gems_upgrade_price = models.CharField(
        verbose_name=u'доп. цены в гемах',
        max_length=64,
        blank=True,
    )
    max_to_buy = models.IntegerField(
        verbose_name=u'максимально возможное количество покупки',
        blank=True,
        default=0,
    )
    max_hp = models.IntegerField(
        verbose_name=u'максимальный запас здоровья',
        blank=True,
        default=5,
    )
    add_during_registration = models.BooleanField(
        verbose_name=u'добавить пользователю при регистрации',
        blank=True,
        default=False,
        db_index=True,
    )

    def __unicode__(self):
        return self.name_ru

    class Meta(object):
        verbose_name = u'предмет'
        verbose_name_plural = u'предметы'


class Inventory(models.Model):
    user = models.ForeignKey(
        Users,
        verbose_name=u'пользователь',
        related_name='item_owner',
    )
    item = models.ForeignKey(
        Items,
        verbose_name=u'предмет',
        related_name='users_item',
    )
    count = models.IntegerField(
        verbose_name=u'количество',
        blank=True,
        default=1,
    )
    equipped = models.IntegerField(
        verbose_name=u'экипировано',
        blank=True,
        default=0,
        db_index=True,
    )
    is_suite = models.BooleanField(
        verbose_name=u'скафандр',
        blank=True,
        default=False,
    )
    hp = models.IntegerField(
        verbose_name=u'запас здоровья',
        blank=True,
        default=5,
        db_index=True,
    )
    hp_time = models.DateTimeField(
        verbose_name=u'время обновления HP',
        blank=True,
        default=datetime.datetime(1988, 1, 1, 11, 40),
    )

    def __unicode__(self):
        socNetId = str(self.user.soc_net_id)
        return socNetId + ' - ' + self.item.name_ru

    class Meta(object):
        verbose_name = u'инвентарь'
        verbose_name_plural = u'инвентари'


class Achievements(models.Model):
    code = models.CharField(
        verbose_name=u'Код для доступа',
        max_length=16,
        blank=False,
        unique=True,
        db_index=True,
    )
    sort = models.IntegerField(
        verbose_name=u'Сортировка',
        default=0,
        blank=False,
        db_index=True,
    )
    name_ru = models.CharField(
        verbose_name=u'название (RU)',
        max_length=48,
        blank=False,
        null=True,
        default='',
    )
    name_en = models.CharField(
        verbose_name=u'название (EN)',
        max_length=48,
        blank=False,
        null=True,
        default='',
    )
    description_ru = models.CharField(
        verbose_name=u'описание (RU)',
        max_length=512,
        blank=True,
        null=True,
        default='',
    )
    description_en = models.CharField(
        verbose_name=u'описание (EN)',
        max_length=512,
        blank=True,
        null=True,
        default='',
    )
    image = models.CharField(
        verbose_name=u'image',
        max_length=32,
        blank=False,
    )

    def __unicode__(self):
        return self.name_ru

    class Meta(object):
        verbose_name = u'достижение'
        verbose_name_plural = u'достижения'


class UsersAchievements(models.Model):
    achievement = models.ForeignKey(
        Achievements,
        verbose_name=u'достижение'
    )
    user = models.ForeignKey(
        Users,
        verbose_name=u'пользователь'
    )

    def __unicode__(self):
        user = unicode(self.user)
        achievement = unicode(self.achievement)
        return user + ' - ' + achievement

    class Meta(object):
        verbose_name = u'полученное достижение'
        verbose_name_plural = u'полученные достижения'


class TrophiesCollections(models.Model):
    code = models.CharField(
        verbose_name=u'код для доступа',
        max_length=16,
        blank=False,
        unique=True,
        db_index=True,
    )
    sort = models.IntegerField(
        verbose_name=u'сортировка',
        default=0,
        blank=False,
        db_index=True,
    )
    name_ru = models.CharField(
        verbose_name=u'название (RU)',
        max_length=48,
        blank=False,
        null=True,
        default='',
    )
    name_en = models.CharField(
        verbose_name=u'название (EN)',
        max_length=48,
        blank=False,
        null=True,
        default='',
    )

    def __unicode__(self):
        return self.name_ru

    class Meta(object):
        verbose_name = u'коллекция трофеев'
        verbose_name_plural = u'коллекции трофеев'


class Trophies(models.Model):
    trophies_collection = models.ForeignKey(
        TrophiesCollections,
        verbose_name=u'коллекция трофеев',
        null=True,
        default=None,
    )
    code = models.CharField(
        verbose_name=u'код доступа',
        max_length=32,
        null=True,
        default=None,
        db_index=True,
        unique=True,
    )
    sort = models.IntegerField(
        verbose_name=u'сортировка',
        default=0,
        blank=False,
        db_index=True,
    )
    name_ru = models.CharField(
        verbose_name=u'название (RU)',
        max_length=48,
        blank=False,
        null=True,
        default='',
    )
    name_en = models.CharField(
        verbose_name=u'название (EN)',
        max_length=48,
        blank=False,
        null=True,
        default='',
    )
    image = models.CharField(
        verbose_name=u'image',
        max_length=32,
        blank=False,
        default='',
    )
    symbol = models.CharField(
        verbose_name=u'symbol',
        max_length=32,
        blank=False,
        default='',
    )

    def __unicode__(self):
        return self.name_ru

    class Meta(object):
        verbose_name = u'трофей'
        verbose_name_plural = u'трофеи'


class UsersTrophies(models.Model):
    trophy = models.ForeignKey(
        Trophies,
        verbose_name=u'трофей'
    )
    user = models.ForeignKey(
        Users,
        verbose_name=u'пользователь'
    )

    def __unicode__(self):
        user = unicode(self.user)
        trophy = unicode(self.trophy)
        return user + ' - ' + trophy

    class Meta(object):
        verbose_name = u'полученный трофей'
        verbose_name_plural = u'полученные трофеи'


class GameSessions(models.Model):
    class DIFFICULTY(object):
        EASY = 'easy'
        MEDIUM = 'medium'
        HARD = 'hard'
        ENDLESS = 'endless'

    DIFFICULTIES_LIST = (DIFFICULTY.EASY, DIFFICULTY.MEDIUM, DIFFICULTY.HARD, DIFFICULTY.ENDLESS)
    DIFFICULTIES_CHOICES = (
        (DIFFICULTY.EASY, u'Легко'),
        (DIFFICULTY.MEDIUM, u'Средне'),
        (DIFFICULTY.HARD, u'Сложно'),
        (DIFFICULTY.ENDLESS, u'Бесконечно')
    )

    user = models.ForeignKey(
        Users,
        verbose_name=u'пользователь'
    )
    trophy_flag = models.BooleanField(
        verbose_name=u'Флаг трофея',
        blank=True,
        default=False,
    )
    start_time = models.DateTimeField(
        verbose_name=u'Время начала',
        blank=False,
        null=True,
        default=None,
        db_index=True,
    )
    difficulty = models.CharField(
        verbose_name=u'уровень сложности',
        max_length=8,
        blank=False,
        default=DIFFICULTY.MEDIUM,
        choices=DIFFICULTIES_CHOICES,
    )

    def __unicode__(self):
        return unicode(self.user.soc_net_id) + u'_' + unicode(self.id)

    class Meta(object):
        verbose_name = u'игровая сессия'
        verbose_name_plural = u'игровые сессии'


class ShopStatistics(models.Model):
    item = models.ForeignKey(
        Items,
        verbose_name=u'предмет',
        unique=True,
    )
    count_global = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=u'покупок всего'
    )
    count_day = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=u'покупок за день'
    )
    count_week = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=u'покупок за неделю'
    )
    count_month = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=u'покупок за месяц'
    )

    def __unicode__(self):
        return unicode(self.item)

    class Meta(object):
        verbose_name = u'статистика покупок'
        verbose_name_plural = u'статистики покупок'


class BankStatistics(models.Model):
    coins = models.IntegerField(
        blank=False,
        verbose_name=u'сумма',
        unique=True,
        db_index=True,
    )
    count_global = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=u'покупок всего'
    )
    count_day = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=u'покупок за день'
    )
    count_week = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=u'покупок за неделю'
    )
    count_month = models.IntegerField(
        blank=True,
        default=0,
        verbose_name=u'покупок за месяц'
    )

    def __unicode__(self):
        return unicode(self.coins)

    class Meta(object):
        verbose_name = u'статистика банка'
        verbose_name_plural = u'статистики банка'


class StarsStatistics(models.Model):
    stars = models.IntegerField(
        verbose_name=u'множитель',
        blank=False,
        db_index=True,
        unique=True,
    )
    count = models.IntegerField(
        verbose_name=u'количество пользователей',
        blank=True,
        default=0,
    )
    percents = models.FloatField(
        verbose_name=u'процент пользователей',
        blank=True,
        default=0,
    )

    def __unicode__(self):
        return unicode(self.stars)

    class Meta(object):
        verbose_name = u'статистика множителей'
        verbose_name_plural = u'статистики множителей'


class AchievementsStatistics(models.Model):
    achievement = models.ForeignKey(
        Achievements,
        verbose_name=u'достижение',
        unique=True,
    )
    count = models.IntegerField(
        verbose_name=u'количество пользователей',
        blank=True,
        default=0,
    )
    percents = models.FloatField(
        verbose_name=u'процент пользователей',
        blank=True,
        default=0,
    )

    def __unicode__(self):
        return unicode(self.achievement)

    class Meta(object):
        verbose_name = u'статистика достижений'
        verbose_name_plural = u'статистики достижений'


class GameSessionsLog(models.Model):
    user = models.ForeignKey(
        Users,
        verbose_name=u'Пользователь',
    )
    time = models.DateTimeField(
        verbose_name=u'Время фиксации по UTC',
        blank=False,
        default=datetime.datetime(1988, 1, 1, 11, 40),
        db_index=True,
    )
    session_time = models.IntegerField(
        verbose_name=u'Секунд между init и finish',
        blank=False,
        default=0,
    )
    difficulty = models.IntegerField(
        verbose_name=u'Уровень сложности',
        blank=False,
        default=0,
    )
    win = models.BooleanField(
        verbose_name=u'Была ли победа',
        blank=False,
        default=False,
    )
    bronze_coins = models.IntegerField(
        verbose_name=u'Бронзовые монеты',
        blank=False,
        default=0,
    )
    silver_coins = models.IntegerField(
        verbose_name=u'Серебряные монеты',
        blank=False,
        default=0,
    )
    gold_coins = models.IntegerField(
        verbose_name=u'Золотые монеты',
        blank=False,
        default=0,
    )
    coins = models.IntegerField(
        verbose_name=u'Сумма монет',
        blank=False,
        default=0,
    )
    score = models.IntegerField(
        verbose_name=u'Счет',
        blank=False,
        default=0,
    )
    trophy_found = models.BooleanField(
        verbose_name=u'Найден ли трофей',
        blank=False,
        default=False,
    )
    cola = models.IntegerField(
        verbose_name=u'Кола',
        blank=False,
        default=0,
    )
    distance = models.IntegerField(
        verbose_name=u'Дистанция',
        blank=False,
        default=0,
    )
    booster_count = models.IntegerField(
        verbose_name=u'Ускорители',
        blank=False,
        default=0,
    )
    power_shield_count = models.IntegerField(
        verbose_name=u'Неуязвимости',
        blank=False,
        default=0,
    )
    heart_count = models.IntegerField(
        verbose_name=u'Сердца',
        blank=False,
        default=0,
    )
    shield_count = models.IntegerField(
        verbose_name=u'Щиты',
        blank=False,
        default=0,
    )
    pause_seconds = models.IntegerField(
        verbose_name=u'Секунд на паузе',
        blank=False,
        default=0,
    )
    bonus_seconds = models.IntegerField(
        verbose_name=u'Секунд в бонусе',
        blank=False,
        default=0,
    )
    seconds = models.IntegerField(
        verbose_name=u'Секунд в игре',
        blank=False,
        default=0,
    )
    max_velocity = models.IntegerField(
        verbose_name=u'Максимальная скорость',
        blank=False,
        default=0,
    )


class GameSessionsStatistics(models.Model):
    name = models.CharField(
        verbose_name=u'поле',
        max_length=32,
        blank=False,
        db_index=True,
        unique=True,
        default='',
    )
    sort = models.IntegerField(
        verbose_name=u'Сортировка',
        blank=True,
        default=0,
    )
    description = models.CharField(
        verbose_name=u'признак',
        max_length=140,
        blank=False,
        default='',
    )
    last_check = models.DateTimeField(
        verbose_name=u'время последней проверки',
        blank=True,
        default=datetime.datetime(1988, 1, 1, 11, 40)
    )
    sum_value = models.BigIntegerField(
        verbose_name=u'сумма',
        blank=True,
        default=0,
    )
    n_value = models.IntegerField(
        verbose_name=u'количество наблюдений',
        blank=True,
        default=0,
    )
    min_value = models.IntegerField(
        verbose_name=u'минимум',
        blank=True,
        default=0,
    )
    max_value = models.IntegerField(
        verbose_name=u'максимум',
        blank=True,
        default=0,
    )
    avg_value = models.FloatField(
        verbose_name=u'средяя',
        blank=True,
        default=0.0,
    )

    def __unicode__(self):
        return unicode(self.name)

    class Meta(object):
        verbose_name = u'статистика игр'
        verbose_name_plural = u'статистики игр'


class ReferrersStatistics(models.Model):
    referrer = models.CharField(
        verbose_name=u'реферрер',
        max_length=64,
        blank=False,
        db_index=True,
        unique=True,
        default='',
    )
    count = models.IntegerField(
        verbose_name=u'количество пользователей',
        blank=True,
        default=0,
    )
    percents = models.FloatField(
        verbose_name=u'процент пользователей',
        blank=True,
        default=0,
    )

    def __unicode__(self):
        return unicode(self.referrer)

    class Meta(object):
        verbose_name = u'статистика реферреров'
        verbose_name_plural = u'статистики реферреров'


class UserNews(models.Model):
    NEWS_TYPES = (
        (1, 'Друг прислал подарок'),
        (2, 'Акция'),
        (3, 'Общая информация'),
    )

    MAX_NEWS_TYPE = 3

    user = models.ForeignKey(
        Users,
        verbose_name=u'пользователь',
        related_name=u'user',
        blank=True,
        null=True,
        default=None,
        db_index=True,
    )
    time = models.DateTimeField(
        verbose_name=u'время новости',
        blank=False,
        default=datetime.datetime(1988, 1, 1, 11, 40),
        db_index=True,
    )
    expires_time = models.DateTimeField(
        verbose_name=u'время устаревания новости',
        blank=False,
        default=datetime.datetime(1988, 1, 1, 11, 40),
        db_index=True,
    )
    news_type = models.SmallIntegerField(
        verbose_name=u'тип новости',
        blank=False,
        choices=NEWS_TYPES,
    )
    additional_data = models.TextField(
        verbose_name=u'дополнительные данные',
        blank=True,
        default=u'',
    )

    def __unicode__(self):
        return unicode(self.user) + ': ' + unicode(self.time)

    class Meta(object):
        verbose_name = u'новость'
        verbose_name_plural = u'новости'


class UserNewsRead(models.Model):
    user = models.ForeignKey(
        Users,
        verbose_name=u'пользователь',
    )
    news = models.ForeignKey(
        UserNews,
        verbose_name=u'новость'
    )
    shared = models.BooleanField(
        verbose_name=u'лайк',
        default=False,
        db_index=True,
    )
